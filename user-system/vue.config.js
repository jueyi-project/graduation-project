module.exports = {
    publicPath: "/",
    chainWebpack: (config) => {
        const svgRule = config.module.rule('svg');
        svgRule.uses.clear();
        svgRule
            .use('babel-loader')
            .loader('babel-loader')
            .end()
            .use('vue-svg-loader')
            .loader('vue-svg-loader');
    },
    outputDir: 'dist',
    devServer: {
        port: 80,
        host: '0.0.0.0',
        https: false,
        open: true,
        proxy: {
            '/api': {
                target: 'https://localhost:8443',
                // target: 'https://jueyi.xyz:8443',
                changeOrigin: true,
                ws: true,
                pathRewrite: {
                    '^/api': '',
                }
            },
            '/gdWeather': {
                target: 'https://restapi.amap.com/v3',
                changeOrigin: true,
                pathRewrite: {
                    '^/gdWeather': ''
                }
            }
        }
    }
}
