import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import ElementUI from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'
import './assets/css/font.css'
import './assets/css/reset.css'
import './assets/css/swiper.min.css'
import "./assets/css/iconfont.css";
// 引入进度条样式
import 'nprogress/nprogress.css'
import VueParticles from "vue-particles"
// 动态滚动
import scroll from 'vue-seamless-scroll'
// 引入图表
import VCharts from 'v-charts'
// 炫酷图标,转换率
import dataV from '@jiaminghi/data-view'
// 下面的两个没用，可以去了
import axios from "axios"
import helloWorld from './utils/helloworld'

Vue.config.productionTip = false;
Vue.use(ElementUI);
Vue.use(VueParticles);
Vue.use(scroll)
Vue.use(VCharts)
Vue.use(dataV)
Vue.prototype.$axios = axios;
Vue.prototype.$helloWorld = helloWorld;

new Vue({
    router,
    store,
    render: h => h(App)
}).$mount('#app');

