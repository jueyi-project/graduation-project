import Vue from 'vue'
import VueRouter from 'vue-router'
// 商家端布局页
import Layout from '@/views/seller/layout'
// 引入nprogress 进度条插件
import NProgress from 'nprogress'

Vue.use(VueRouter);
// 商家端的路由
export const constRouter = [
    {
        path: '/seller/system/workbench',
        component: Layout, //应用布局页
        children: [
            {
                path: '',
                component: () => import('@/views/seller/workbench'),
                name: 'workbench',
                meta: {
                    title: " 工作台",
                    icon: 'el-icon-s-home',
                    hidden: false
                }
            }
        ]
    },
    {
        path: '/seller/system/analyze',
        component: Layout,
        meta: {
            title: "数据分析",
            icon: 'el-icon-s-marketing',
            hidden: false
        },
        children: [
            {
                path: '',
                component: () => import('@/views/seller/analyze'),
                name: 'analyze',
                meta: {
                    title: "主控数据",
                    hidden: false,
                }
            },
            {
                path: 'monitor',
                component: () => import('@/views/seller/analyze/monitor'),
                name: 'monitor',
                meta: {
                    title: "监控数据",
                    hidden: false,
                }
            },
        ]
    },
    {
        path: '/seller/system/goods',
        component: Layout,
        meta: {
            title: "商品管理",
            icon: 'el-icon-s-goods',
            hidden: false,
        },
        children: [
            {
                path: '',
                component: () => import('@/views/seller/goods'),
                name: 'goods',
                meta: {
                    title: "在售商品",
                    hidden: false,
                }
            },
            {

                path: 'waitToShelf',
                component: () => import('@/views/seller/goods/sort'),
                name: 'waitToShelf',
                meta: {
                    title: "待上架商品",
                    hidden: false,
                }
            },
            {
                path: 'recycleBin',
                component: () => import('@/views/seller/goods/sort'),
                name: 'recycleBin',
                meta: {
                    title: "商品回收站",
                    hidden: false,
                }
            },
        ]
    },
    {
        path: '/seller/system/order',
        component: Layout,
        meta: {
            title: "订单管理",
            icon: 'el-icon-user-solid',
            hidden: false,
        },
        children: [
            {
                path: '',
                component: () => import('@/views/seller/order'),
                name: 'orders',
                meta: {
                    title: "全部订单",
                    hidden: false,
                }
            },
            {
                path: 'waitSend',
                component: () => import('@/views/seller/order'),
                name: 'orders',
                meta: {
                    title: "待发货订单",
                    hidden: false,
                }
            },
            {

                path: 'waitRefund',
                component: () => import('@/views/seller/order'),
                name: 'orders',
                meta: {
                    title: "待退款订单",
                    hidden: false,
                }
            },
        ]
    },
    {
        path: '/seller/system/information',
        component: Layout,
        children: [
            {
                path: '',
                component: () => import('@/views/seller/information'),
                name: 'information',
                meta: {
                    title: "消息通知",
                    icon: 'el-icon-message-solid',
                    hidden: false,
                }
            },
        ]
    }
]

// 总路由
const routes = [
    {
        path: '/',
        component: () => import('../views/home'),
        meta: {title: '佳品乐购-jueyi的毕业设计'}
    },
    {
        path: '/operate',
        component: () => import('../views/operate'),
        redirect: '/operate/login',
        children: [
            {
                path: 'login',
                meta: {title: '登录'},
                component: () => import('../views/operate/loginForm')
            },
            {
                path: 'register',
                meta: {title: '注册'},
                component: () => import('../views/operate/registerForm')
            },
            {
                path: 'forgetPsw',
                meta: {title: '忘记密码'},
                component: () => import('../views/operate/forgetPswForm')
            },
            {
                path: 'beSeller',
                meta: {
                    title: '商家认证',
                    requireAuth: true
                },
                component: () => import('../views/operate/beSellerForm')
            }
        ]
    },
    {
        path: '/userinfo',
        meta: {title: '支付宝账号信息'},
        component: () => import('../views/userinfo'),
    },
    {
        path: '/userCenter',
        meta: {
            requireAuth: true // 添加该字段，表示进入这个路由是需要登录的
        },
        component: () => import('../views/userCenter'),
        children: [
            {
                path: '',
                meta: {title: '全部订单'},
                component: () => import('../views/userCenter/order')
            },
            {
                path: 'waitPay',
                meta: {title: '待付款'},
                component: () => import('../views/userCenter/waitPay')
            },
            {
                path: 'waitSend',
                meta: {title: '待发货'},
                component: () => import('../views/userCenter/waitSend')
            },
            {
                path: 'waitReceive',
                meta: {title: '待收货'},
                component: () => import('../views/userCenter/waitReceive')
            },
            {
                path: 'waitReview',
                meta: {title: '待评论'},
                component: () => import('../views/userCenter/waitReview')
            },
            {
                path: 'personal',
                meta: {title: '个人信息'},
                component: () => import('../views/userCenter/personal'),
            },
            {
                path: 'address',
                meta: {title: '地址管理'},
                component: () => import('../views/userCenter/address')
            },
            {
                path: 'message',
                meta: {title: '消息通知'},
                component: () => import('../views/userCenter/message'),
            }
        ]
    },
    {
        path: '/seller',
        component: () => import('../views/seller'),
        redirect: '/seller/system/workbench',
        children: [
            {
                path: 'login',
                meta: {title: '登录'},
                component: () => import('../views/seller/login'),
            },
            {
                path: 'system',
                redirect: '/seller/system/workbench',
                meta: {requireAuth: true, type: '1'},
                component: () => import('../views/seller/system'),
                children: constRouter
            }
        ]
    },
    {
        path: '/search',
        meta: {title: '搜索结果'},
        component: () => import('../views/search'),

    },
    {
        path: '/goods',
        meta: {title: '商品详情'},
        component: () => import('../views/goods'),

    },
    {
        path: '/cart',
        meta: {
            requireAuth: true,
            title: '购物车'
        },
        component: () => import('../views/cart')
    },
    {
        path: '/trade',
        meta: {requireAuth: true},
        component: () => import('@/views/trade'),
        redirect: '/trade/fromCart',
        children: [
            {
                path: 'fromCart',
                meta: {
                    title: '下单',
                },
                component: () => import('@/views/trade/cartToTrade')
            },
            {
                path: 'fromGoods',
                meta: {
                    title: '下单',
                },
                component: () => import('@/views/trade/goodsToTrade')
            }
        ]
    },
    {
        path: '/pay',
        meta: {
            requireAuth: true
        },
        component: () => import('../views/pay'),
        children: [
            {
                path: '',
                component: () => import('../views/pay/pay')
            },
            {
                path: 'alipay',
                component: () => import('../views/pay/alipay')
            },
            {
                path: 'paySuccess',
                meta: {title: '支付成功'},
                component: () => import('../views/pay/paySuccess')
            }
        ]
    },
    {
        path: '/403',
        meta: {title: '禁止访问'},
        component: () => import('../views/403')
    },
    {
        path: '/404',
        meta: {title: '找不到了'},
        component: () => import('../views/404')
    },
    {
        path: '/test',
        component: () => import('../views/test')
    },
    {
        path: '*',
        redirect: '/404'
    }
];


const router = new VueRouter({
    mode: 'history',
    base: process.env.BASE_URL,
    routes: routes
});

const originalPush = VueRouter.prototype.push;
VueRouter.prototype.push = function push(location) {
    return originalPush.call(this, location).catch(err => err);
};

// 简单配置  进度条
NProgress.inc(0.2)
NProgress.configure({easing: 'ease', speed: 500, showSpinner: false})

// 路由判断登录 根据路由配置文件的参数
router.beforeEach((to, from, next) => {
    NProgress.start();
    //判断是否有标题
    if (to.meta.title) {
        document.title = to.meta.title;
    } else {
        document.title = '佳品乐购';
    }
    //判断是否需要登录
    if (to.matched.some(record => record.meta.requireAuth)) {
        // 需要登录权限,判断是否登录
        if (router.app.$options.store.state.user.username !== null) {
            // 已经登录过了，判断是否需要商家权限
            if (to.matched.some(record => record.meta.type === '1')) {
                //需要商家权限，判断是否拥有商家权限
                if (router.app.$options.store.state.user.type === '1') {
                    // 拥有商家权限
                    next();
                } else {
                    //没有商家权限
                    next({
                        path: '/403',
                    })
                }
            } else {
                //不需要商家权限
                next();
            }
        } else {
            //没有登录，判断是否需要商家权限
            if (to.matched.some(record => record.meta.type === '1')) {
                //需要商家权限
                next({
                    path: '/seller/login',
                    query: {redirect: to.fullPath} // 将跳转的路由path作为参数，登录成功后跳转到该路由
                })
            } else {
                //不需要商家权限
                next({
                    path: '/operate/login',
                    query: {redirect: to.fullPath}
                })
            }
        }
    } else {
        //不需要登录
        next();
    }
});

router.afterEach(() => {
    NProgress.done()
})

export default router
