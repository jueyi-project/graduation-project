import request from './request'

// 获取订单信息列表
export const getOrderItemList = () => request({url: '/cart/findNewOrderItemList', method: 'get'});

// 获取购物车被选中商品的优惠价格
export const getPreferential = () => request({url: '/cart/preferential', method: 'get'});

//加入到购物车
export const addShoppingCart = (params) => request({url: '/cart/buy', method: 'get', params});

//修改购物车商品数量
export const updateCartProductNumber = (params) => request({url: `/cart/updateNum`, method: 'get', params});

//是否需要发票
export const updateInvoice = (params) => request({url: `/cart/updateInvoice`, method: 'get', params});

//买家留言
export const updateBuyerMsg = (params) => request({url: `/cart/updateBuyerMsg`, method: 'get', params});

//获取购物车中所有商品
export const shoppingCartList = () => request({url: '/cart/findCartList', method: 'get'});

//更新购物车勾选状态
export const updateCheckedByList = (data) => request({url: `/cart/updateCheckedByList`, method: 'post', data});

//删除购物车中被勾选的商品
export const shoppingCartDeleteProduct = () => request({url: '/cart/deleteCheckedCart', method: 'get'});


