import axios from 'axios'

axios.defaults.withCredentials = true;

function request(config) {
    const instance = axios.create({
        baseURL: '/api',
        timeout: 20000
    })

    instance.interceptors.response.use(res => {
        return res.data;
    }, error => {
        return Promise.reject(error);
    })

    return instance(config);
}

export function request_gaodeMap(config) {
    const instance = axios.create({
        baseURL: '/gdWeather',
        timeout: 20000
    })
    instance.interceptors.response.use(res => {
        return res.data;
    }, error => {
        return Promise.reject(error);
    })

    return instance(config);
}

export default request;

