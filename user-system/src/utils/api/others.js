import request, {request_gaodeMap} from './request'

//获取验证码
export const getCode = (params) => request({url: '/sms/userRegist', method: "get", params});

//登录后，不需要填手机号，就能获取验证码
export const getCodeAfterLogin = () => request({url: '/sms/userUpdatePhone', method: "get"});

//注册
export const registerUser = (data) => request({url: "/register/addUser", method: "post", data});

//申请成为商家
export const toBeSeller = (data) => request({url: "/register/toBeSeller", method: "post", data});

//支付宝付款
export const payMoney = (params) => request({url: '/payMoney', method: "get", params});

//更新普通订单付款状态
export const updatePayOrder = (params) => request({url: '/pay/updatePayOrder', method: "get", params});

//上传头像
export const uploadAvatar = (data) => request({url: "/oss/uploadAvatar", method: "post", data});

//获取定位
export const getAdcode = (params) => request_gaodeMap({url: "/ip", method: "get", params});

//获取天气
export const getGodeWeather = (params) => request_gaodeMap({url: "/weather/weatherInfo", method: "get", params});

