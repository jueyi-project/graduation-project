import request from './request'

//根据username查出未删除的order
export const findOrders = (params) => request({
    url: '/userOrder/findOrders', method: 'get', params
});

//根据前端返回的orderList返回orderAndItemsList
export const findOrderAndItems = (params, data) => request({
    url: `/userOrder/findOrderAndItems`, method: 'post', params, data
});

//根据订单状态查找订单，0-待付款，1-待发货，2-待收货，3-待评价，4-已关闭
export const findOrdersByStatus = (params) => request({
    url: '/userOrder/findOrdersByStatus', method: 'get', params
});



