import request from './request'

// 获取收货地址列表
export const getAddressList = (params) => request({url: '/address/findAddressByUsername', method: "get", params});

// 查找收货地址
export const findPCA = (data) => request({url: "/address/findPCA", method: "post", data});

//添加收货地址数据
export const addAddress = (data) => request({url: "/address/addAddress", method: "post", data});

//删除地址列表的第id条
export const delAddressById = (params) => request({url: '/address/deleteAddressById', method: "get", params});

//修改默认地址设置
export const toDefault = (params) => request({url: '/address/changeToDefault', method: "get", params});

//修改更新收货地址数据
export const updateAddress = (data) => request({url: "/address/updateAddress", method: "post", data});


