import request from './request'

// 搜索商品
export const reqGoodsList = (data) => request({url: `/goods/searchGoods`, method: 'post', data});

// 商品详情请求
export const getProductDetail = (params) => request({url: "/goods/goodsDetails", method: "get", params});

// 商品优惠
export const getPreferential = (params) => request({url: "/goods/getPreferential", method: "get", params});

//用skuId查询商品是否参加优惠活动
export const hasPreferential = (params) => request({url: "/goods/hasPreferential", method: "get", params});

// 获取首页懒加载商品数据api
export const getLayerShop = (params) => request({url: "/goods/fetchData", method: "get", params});

// 获取首页轮播图区域数据
export const getCarouselList = () => request({url: "/goods/carousel", method: "get"});





