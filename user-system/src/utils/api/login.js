import request from './request'


//买家登录
export const Login = (data) => request({url: "/login", method: "post", data});

//买家退出登录
export const Logout = () => request({url: '/login/logout', method: "get"});

//卖家密码登录
export const SellerLoginByPsw = (data) => request({url: "/login/sellerLoginByPsw", method: "post", data});

//卖家验证码登录
export const SellerLoginByYZM = (data) => request({url: "/login/sellerLoginByYZM", method: "post", data});

