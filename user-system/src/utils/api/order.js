import request from './request'

//从购物车生成订单
export const makeOrder = (data) => request({url: "/order/saveOrder", method: "post", data});

//从立即购买生成订单
export const saveOrderNow = (data) => request({url: "/order/saveOrderNow", method: "post", data});

//取消未付款订单
export const cancelOrder = (params) => request({url: "/order/cancelOrder", method: "get", params});

//删除完成的订单
export const delOrder = (params) => request({url: "/order/delOrder", method: "get", params});
