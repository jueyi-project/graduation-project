import request from './request'

//获取支付宝用户信息
export const getUserInfo = (params) => request({url: '/userinfo', method: "get", params});

//获取当前登录用户信息
export const getUser = () => request({url: '/userinfo/getUser', method: "get"});

//获取用户手机号
export const getUserPhone = (params) => request({url: '/userinfo/getUserPhone', method: "get", params});

//获取用户ip
export const getUserIP = () => request({url: '/userinfo/getUserIP', method: "get"});

//修改真实姓名
export const updateName = (params) => request({url: '/userinfo/updateName', method: "get", params});

//修改头像
export const updateHeadPic = (params) => request({url: '/userinfo/updateHeadPic', method: "get", params});

//修改手机
export const updatePhone = (params) => request({url: '/userinfo/updatePhone', method: "get", params});

//修改密码
export const updatePassword = (params) => request({url: '/userinfo/updatePassword', method: "get", params});

//修改密码
export const updatePsw = (params) => request({url: '/userinfo/updatePsw', method: "get", params});


