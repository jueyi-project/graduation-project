export function debounce(fn, dur) {
    dur = dur || 1000;
    var timer;
    return function () {
        let args = arguments
        let content = this;
        clearTimeout(timer);
        timer = setTimeout(() => {
            fn.apply(content, args);
        }, dur)
    }
}


