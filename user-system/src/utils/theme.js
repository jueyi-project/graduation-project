const Theme = 'theme';

export const themeColor = [
    {
        aside: 'background-image: linear-gradient(#001529, #001529);',
        header: 'background-image: linear-gradient(to right,#ffffff, #ffffff);',
        container: 'background-image: linear-gradient(to bottom right,#ffffff,#ffffff);'
    },
    {
        aside: 'background-image: linear-gradient(#00BCFF, #00BCFF);',
        header: 'background-image: linear-gradient(to right,#D2F2F1, #D2F2F1);',
        container: 'background-image: linear-gradient(to bottom right,#D2F2F1,#D2F2F1);'
    },
    {
        aside: 'background-image: linear-gradient(#9E25B0, #9E25B0);',
        header: 'background-image: linear-gradient(to right,#EBD4F1, #EBD4F1);',
        container: 'background-image: linear-gradient(to bottom right,#EBD4F1,#EBD4F1);'
    },
    {
        aside: 'background-image: linear-gradient(#E13998, #E13998);',
        header: 'background-image: linear-gradient(to right,#F9D6E9, #F9D6E9);',
        container: 'background-image: linear-gradient(to bottom right,#F9D6E9,#F9D6E9);'
    },
    {
        aside: 'background-image: linear-gradient(#E54E43, #E54E43);',
        header: 'background-image: linear-gradient(to right,#FBD3D0, #FBD3D0);',
        container: 'background-image: linear-gradient(to bottom right,#FBD3D0,#FBD3D0);'
    },
    {
        aside: 'background-image: linear-gradient(#E13998, #F0F0F0);',
        header: 'background-image: linear-gradient(to right,#EDC6DC, #ffffff);',
        container: 'background-image: linear-gradient(to bottom right,#E13998,#F0F0F0);'
    },
];

export function getTheme() {
    return window.localStorage.getItem(Theme)
}

export function setTheme(token) {
    return window.localStorage.setItem(Theme, token)
}

export function removeTheme() {
    return window.localStorage.removeItem(Theme)
}