import Vue from 'vue'
import Vuex from 'vuex'

import createPersistedState from 'vuex-persistedstate';
import user from './modules/user';
import search from './modules/search'
import tagsView from './modules/tagsView'

Vue.use(Vuex)

export default new Vuex.Store({
    plugins: [createPersistedState({
        // key是存储数据的键名
        // paths是存储state中的那些数据，如果是模块下具体的数据需要加上模块名称，如user.token
        // 修改state后触发才可以看到本地存储数据的的变化。
        // reducer: 指定需要缓存的状态,用于持久化存储store中的部分状态信息
        storage: window.sessionStorage,
        // key: user,
        // reducer()函数的data参数是完整的state对象
        reducer(data) {
            return {
                // 设置只存储user模块中的状态
                user: data.user
            }
        }
    })],
    modules: {
        search,
        user,
        tagsView
    }
})
