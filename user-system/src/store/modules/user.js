const state = {
    username: null,
    type: null
}

//mutations
const mutations = {
    saveUser(state, value) {
        state.username = value.username
        state.type = value.type
    },
    logout(state) {
        state.username = null;
        state.type = null;
    }
}


export default {
    namespaced: true,
    state,
    mutations,
}