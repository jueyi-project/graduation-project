import {reqGoodsList} from '../../utils/api/goods'

const state={
    goodsList:[],
    //数据总条数
    total:0
}
const actions={
     async  getGoodsList({commit},value){
            const result=await reqGoodsList(value)
                commit('GETGOODSLIST',result)
        }
}
const mutations={
    GETGOODSLIST(state,value){
        state.goodsList=value.rows
        state.total=value.total
    }
}
export default{
    namespaced:true,
    state,
    actions,
    mutations
}