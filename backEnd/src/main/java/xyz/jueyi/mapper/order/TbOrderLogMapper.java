package xyz.jueyi.mapper.order;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import xyz.jueyi.entity.order.TbOrderLog;

@Mapper
public interface TbOrderLogMapper extends BaseMapper<TbOrderLog> {

}
