package xyz.jueyi.mapper.admin;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import xyz.jueyi.entity.admin.TbAdmin;

@Mapper
public interface TbAdminMapper extends BaseMapper<TbAdmin> {

}
