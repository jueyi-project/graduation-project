package xyz.jueyi.mapper.user;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import xyz.jueyi.entity.user.TbAddress;

@Mapper
public interface TbAddressMapper extends BaseMapper<TbAddress> {

}
