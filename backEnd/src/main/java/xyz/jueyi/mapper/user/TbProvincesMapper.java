package xyz.jueyi.mapper.user;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import xyz.jueyi.entity.user.TbProvinces;

@Mapper
public interface TbProvincesMapper extends BaseMapper<TbProvinces> {

}
