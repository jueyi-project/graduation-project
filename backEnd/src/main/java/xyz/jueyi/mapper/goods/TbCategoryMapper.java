package xyz.jueyi.mapper.goods;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import xyz.jueyi.entity.goods.TbCategory;

@Mapper
public interface TbCategoryMapper extends BaseMapper<TbCategory> {

}
