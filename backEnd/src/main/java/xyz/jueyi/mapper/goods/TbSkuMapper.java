package xyz.jueyi.mapper.goods;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import xyz.jueyi.entity.goods.TbSku;

import java.util.List;

@Mapper
public interface TbSkuMapper extends BaseMapper<TbSku> {
    @Select("select * from tb_sku where category_name like '%${str}%' OR brand_name like '%${str}%' ")
    public List<TbSku> search(@Param("str") String str);


    /**
     * 扣减库存的方法
     *
     * @param id
     * @param num
     */
    @Select("update tb_sku set num=num-${num} where id=${id}")
    public void deductionstock(@Param("id") String id, @Param("num") int num);

    /**
     * 增加销量
     *
     * @param id
     * @param num
     */
    @Select("update tb_sku set sale_num=sale_num + ${num} where id=${id}")
    public void addSaleNum(@Param("id") String id, @Param("num") int num);

}
