package xyz.jueyi.mapper.goods;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import xyz.jueyi.entity.goods.TbPreferential;

@Mapper
public interface TbPreferentialMapper extends BaseMapper<TbPreferential> {

}
