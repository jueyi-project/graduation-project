package xyz.jueyi.service.user;

import com.baomidou.mybatisplus.extension.service.IService;
import xyz.jueyi.entity.user.TbAreas;

import java.util.List;

public interface AreasService extends IService<TbAreas> {


    List<TbAreas> findAreasByCityId(String cityid);
}
