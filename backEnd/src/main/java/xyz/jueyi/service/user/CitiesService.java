package xyz.jueyi.service.user;

import com.baomidou.mybatisplus.extension.service.IService;
import xyz.jueyi.entity.user.TbCities;

import java.util.List;

public interface CitiesService extends IService<TbCities> {


    List<TbCities> findCitiesByProvinceId(String provinceid);
}
