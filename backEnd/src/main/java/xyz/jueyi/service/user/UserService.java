package xyz.jueyi.service.user;

import com.baomidou.mybatisplus.extension.service.IService;
import xyz.jueyi.entity.user.TbUser;

import java.util.List;

public interface UserService extends IService<TbUser> {


    TbUser findById(String username);

    List<TbUser> findAll();

    List<TbUser> findByPhone(String phone);

    void add(TbUser user);

    void updateName(String name, String username);

    void updateHeadPic(String headPic, String username);

    void updatePhone(String phone, String username);

    void updatePsw(String password, String username);
}
