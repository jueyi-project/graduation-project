package xyz.jueyi.service.user;

import com.baomidou.mybatisplus.extension.service.IService;
import xyz.jueyi.entity.user.TbAddress;

import java.util.List;

public interface AddressService extends IService<TbAddress> {


    List<TbAddress> findAddressByUsername(String username);

    TbAddress findByUsernameAndDefault(String username);

}
