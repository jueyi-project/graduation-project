package xyz.jueyi.service.user;

import java.util.List;
import java.util.Map;

public interface CartService {
    List<Map<String, Object>> findCartList(String username);

    void addGoods(String username, String skuId, Integer num);

    void updateNum(String username, String skuId, Integer num);

    void updateInvoice(String username, String skuId);

    void updateBuyerMessage(String username, String skuId, String msg);

    void updateCheckedByList(String username, List<String> skuList);

    void deleteCheckedCart(String username);

    int preferential(String username);

    List<Map<String, Object>> findNewOrderItemList(String username);
}
