package xyz.jueyi.service.user;

import com.baomidou.mybatisplus.extension.service.IService;
import xyz.jueyi.entity.PageResult;
import xyz.jueyi.entity.user.TbApplication;

import java.util.List;

public interface ApplicationService extends IService<TbApplication> {
    TbApplication findApplicationByState(String username, String state);

    PageResult<TbApplication> getNotReviewed(int page, int size);

    PageResult<TbApplication> getReviewed(int page, int size);
}
