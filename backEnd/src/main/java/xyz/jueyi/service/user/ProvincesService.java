package xyz.jueyi.service.user;

import com.baomidou.mybatisplus.extension.service.IService;
import xyz.jueyi.entity.user.TbProvinces;

import java.util.List;

public interface ProvincesService extends IService<TbProvinces> {


    List<TbProvinces> findAll();
}
