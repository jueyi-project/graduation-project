package xyz.jueyi.service.goods;

import com.baomidou.mybatisplus.extension.service.IService;
import xyz.jueyi.entity.PageResult;
import xyz.jueyi.entity.goods.TbAd;

import java.util.List;

public interface AdService extends IService<TbAd> {
    List getCarousels();

    PageResult<TbAd> getAds(int page, int size);
}
