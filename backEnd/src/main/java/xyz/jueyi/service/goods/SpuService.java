package xyz.jueyi.service.goods;

import com.baomidou.mybatisplus.extension.service.IService;
import xyz.jueyi.entity.goods.TbSpu;

public interface SpuService extends IService<TbSpu> {


    TbSpu findById(String id);
}
