package xyz.jueyi.service.goods;

import com.baomidou.mybatisplus.extension.service.IService;
import xyz.jueyi.entity.PageResult;
import xyz.jueyi.entity.goods.TbCategory;

import java.util.List;
import java.util.Map;

public interface CategoryService extends IService<TbCategory> {

    List<TbCategory> getCategoriesByParentId(int parentId);

}
