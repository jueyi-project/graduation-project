package xyz.jueyi.service.goods;

import com.baomidou.mybatisplus.extension.service.IService;
import xyz.jueyi.entity.PageResult;
import xyz.jueyi.entity.goods.TbPreferential;

public interface PreferentialService extends IService<TbPreferential> {

    int findPreMoneyByCategoryId(Integer categoryId, int money);

    PageResult<TbPreferential> findPage(int page, int size);

    TbPreferential getPreferentialByCategoryId(int categoryId);

    TbPreferential findByCategoryId(int categoryId);
}
