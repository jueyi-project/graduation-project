package xyz.jueyi.service.goods;

import com.baomidou.mybatisplus.extension.service.IService;
import xyz.jueyi.entity.PageResult;
import xyz.jueyi.entity.goods.TbSku;
import xyz.jueyi.entity.order.TbOrderItem;

import java.util.List;

public interface SkuService extends IService<TbSku> {

    PageResult<TbSku> findPage(int page, int size);

    TbSku findById(String id);

    List<TbSku> search(String str);

    PageResult<TbSku> searchPage(String str, int page, int size);

    boolean deductionStock(List<TbOrderItem> orderItems);
}
