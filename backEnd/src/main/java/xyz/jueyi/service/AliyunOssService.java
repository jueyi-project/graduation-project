package xyz.jueyi.service;

import org.springframework.web.multipart.MultipartFile;

public interface AliyunOssService {
    String uploadTest();

    String uploadAvatar(String username, MultipartFile file);

    String uploadCardFront(String username, MultipartFile file);

    String uploadCardBack(String username, MultipartFile file);

    String uploadAdPhoto(MultipartFile file);
}
