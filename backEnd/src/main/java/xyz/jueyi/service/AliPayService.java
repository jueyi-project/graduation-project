package xyz.jueyi.service;

import com.alipay.api.response.AlipaySystemOauthTokenResponse;
import com.alipay.api.response.AlipayUserInfoShareResponse;

/**
 * @Author: jueyi
 * @Description: 支付宝支付
 * @Version: V1.0
 */
public interface AliPayService {

    String alipay(String outTradeNo, String totalAmount, String subject, String body);

    AlipaySystemOauthTokenResponse getToken(String auth_code);

    AlipayUserInfoShareResponse getUserInfo(String accessToken);
}
