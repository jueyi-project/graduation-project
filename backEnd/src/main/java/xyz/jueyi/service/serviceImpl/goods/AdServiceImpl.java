package xyz.jueyi.service.serviceImpl.goods;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import org.springframework.stereotype.Service;
import xyz.jueyi.entity.PageResult;
import xyz.jueyi.entity.goods.TbAd;
import xyz.jueyi.mapper.goods.TbAdMapper;
import xyz.jueyi.methods.SystemDateUtils;
import xyz.jueyi.service.goods.AdService;

import javax.annotation.Resource;
import java.util.List;

@Service
public class AdServiceImpl extends ServiceImpl<TbAdMapper, TbAd> implements AdService {

    @Resource
    private TbAdMapper adMapper;

    @Override
    public List getCarousels() {
        QueryWrapper<TbAd> queryWrapper = new QueryWrapper<>();
        queryWrapper.select("image").eq("status", "1").lt("start_time", SystemDateUtils.getDate()).ge("end_time", SystemDateUtils.getDate());
        return adMapper.selectMaps(queryWrapper);
    }

    @Override
    public PageResult<TbAd> getAds(int page, int size) {
        PageHelper.startPage(page, size);
        QueryWrapper<TbAd> queryWrapper = new QueryWrapper<>();
        Page<TbAd> pages = (Page<TbAd>) adMapper.selectList(queryWrapper);
        return new PageResult<>(pages.getTotal(), pages.getResult());
    }
}
