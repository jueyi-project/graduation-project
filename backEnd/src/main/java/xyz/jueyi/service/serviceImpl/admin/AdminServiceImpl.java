package xyz.jueyi.service.serviceImpl.admin;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import xyz.jueyi.entity.admin.TbAdmin;
import xyz.jueyi.mapper.admin.TbAdminMapper;
import xyz.jueyi.service.admin.AdminService;

import javax.annotation.Resource;

@Service
public class AdminServiceImpl extends ServiceImpl<TbAdminMapper, TbAdmin> implements AdminService {

    @Resource
    private TbAdminMapper adminMapper;


    @Override
    public TbAdmin findByPhone(String phone) {
        QueryWrapper<TbAdmin> queryWrapper = new QueryWrapper<>();
        return adminMapper.selectOne(queryWrapper.eq("phone", phone));
    }

    @Override
    public TbAdmin findByUsername(String username) {
        QueryWrapper<TbAdmin> queryWrapper = new QueryWrapper<>();
        return adminMapper.selectOne(queryWrapper.eq("login_name", username));
    }


}
