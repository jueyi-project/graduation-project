package xyz.jueyi.service.serviceImpl;

import com.alipay.api.AlipayApiException;
import com.alipay.api.AlipayClient;
import com.alipay.api.DefaultAlipayClient;
import com.alipay.api.request.AlipaySystemOauthTokenRequest;
import com.alipay.api.request.AlipayTradePagePayRequest;
import com.alipay.api.request.AlipayUserInfoShareRequest;
import com.alipay.api.response.AlipaySystemOauthTokenResponse;
import com.alipay.api.response.AlipayUserInfoShareResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import xyz.jueyi.config.AlipayConfig;
import xyz.jueyi.service.AliPayService;

import javax.annotation.Resource;


@Slf4j
@Service
public class AliPayServiceImpl implements AliPayService {

    /**
     * 沙箱账号
     */
    @Resource
    private AlipayConfig alipayConfig;

    /**
     * 真正的卖家账号
     */
    private String appId = "2021002178688118";
    private String gatewayUrl = "https://openapi.alipay.com/gateway.do";
    private String privateKey = "MIIEvAIBADANBgkqhkiG9w0BAQEFAASCBKYwggSiAgEAAoIBAQCCPBLRJlpLZuhaxjzX67GVsRU8WOCKiUIO5+WRecmt6imj2NeAsyRHn6rPngGJtd09+TdrfPXSx35d26naa2j07vdtzR3o+td2pDTQqR1B4hhqglDGL7Q3uJaIYXNrKtiDRBPCcup+E2XQKtaQdDdmuT8blNQNpe2L2VmPwKHyGtNIWAhMQLT7jTpgga42W+Aiw7DPg5Mslp6ZxTOZLEipM0RD0wOW49UhRnfXjhZsW7/wH63T0Ed99Ds+xsYBYer+2UHq5hv72QW/ixBC3UAj1l5pmtwQszXfcfBU2+eZ3NP31lwhZXzl/8S5dTcfFjYibx2JFbZd5R/3Rfkkpx0VAgMBAAECggEAJvV7l4+GkWnedlE5IEqEnEgWFzBrM2YuGbFrbmJlX55eCIK0rO5qGvb7rRISWbTK02A7IO+Hxh8hgUhV/l4FTsgxwmoK2Yg4bmhe2FDOzMerECmdqjvZCJYyiefFFDkD/fiehJvF5CM/YM9/0kTB0fv2kfSqns6VSjGh5iv1BixqSF34hDMxzPjuXeGb3Yt1rxpb7pCBNcVLoLoBPZZauvn1qiPLQUYurzteGJSUU2pXTL2exOadv/frSQQmUQo2BHlHUaiAW8wuP28BTO6jPZEa7UAvSts+1vns/N1P2EILvoLD7RFahat6SPYw2iGuae0pLVJsjtC871gmfTGOPQKBgQDjXAoTfnFRDHJYBrridOEIj0q8NRfEq+8u373POkLUT2zqNS0ATTv2HnHeG0vGTNxTcQ31Cwwq+9XlgM22sv369u9ApchisOSsAcJwN/M0pRE8fgLN/lUKO6OBsi6VTKVtexW4uQ+yHSbbHcwMgsxS62RX5jfr6sYT/pK8sn9aVwKBgQCSo+valNr5A4fzzGmAJgjRTQmi8K0FTY1KEUKpizJQOm1GiMyRUUn1lS7C0PJtDzSyfFCneclxi/SV5Xejf23PAvlt1Ynr2VtZwmp7Ma8WFR028Fh4D+fsEjbD5KeXxnXbMQXq9DUnGoLp8Rr+mee2EOEhCP9TNwZlj4SHu7W4cwKBgGBtAa0A3GhmYfxpZktzDJnHxAeMcPctgHSd2e+QoxVOVkACq4JMP1kD0oFDjGCe6nbNJQE/XTrBshbzCMwqn8MXhaMibJlGssUsvd1ALXqvY18HhoL9Ee4UGyV/f3g8mJCVOkIs4rZE3Nzjh03rD2M/iEArWJ5K5RVHy84R2idjAoGAPyZGiLA4CKrDnnM2L9CxVWlGsBvTdwgpw27hdWATf8dZjdmVtzL5lb/0DkOrUtoMiJzOowDohnSOFQtn1UooQND4F5uxNJc7JtF5x6niy7C2DxW8E6BN8TT6zqumnM8DrCxVbxRR25AoNpT1WhVvWtG0EeTtB6oXoB8JJObqYA8CgYBs2Dul+h20qA8OpL1b2g1Wx4zXr5COnIwoIMHWuW30XQffDxhnFr46J5vgwJYEvdFX0WnHyJTx1iSmTnt+ciPyhRhTI4GwU4rwpjtL0b1e1Pdz35MAs1tQEjv8GOQiSLpOKPHA3r2EaODG+NakVrT7Nv45yYDmKq235/Dz2b7dpQ==";
    private String publicKey = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAgnHpI4bAUQ8b+/Jy+8gIdxt2C0aPq3I5yBtL+grTeV/17KwlPYpxMbBWycUybelgGylXcv1XPXzVyLYJk0CxqetnZTt/E9xax7dIZGKp2D5DLdR3ImANkVgrQaNYk4QVfPb+5p/1btUJBringmreuUTniVpBAf7ry/CCrzcKyziLaulNUb95gXiYnOJ5IQQxeKaY77UASt3HFSC9oSmCUPHHZWyWIfSpH0FfvjJR61IbnfD4SxfzB0BFyzbsFiQsBKLAKKwR3se5pOsxlNk9Zz62kS1CxulBNYEkIQ1+CKmWE+dqzpnGMnCezy8PFAjZkCDqpwxp2f4G2ecW9EfnhwIDAQAB";


    /**
     * 获取token
     *
     * @param auth_code
     * @return
     */
    public AlipaySystemOauthTokenResponse getToken(String auth_code) {
        AlipayClient alipayClient = new DefaultAlipayClient(gatewayUrl,
                appId,
                privateKey,
                "json",
                alipayConfig.getCharset(),
                publicKey,
                alipayConfig.getSign_type());
        AlipaySystemOauthTokenRequest request = new AlipaySystemOauthTokenRequest();
        request.setGrantType("authorization_code");
        request.setCode(auth_code);
        AlipaySystemOauthTokenResponse response = null;
        try {
            response = alipayClient.execute(request);
        } catch (AlipayApiException e) {
            e.printStackTrace();
        }
        return response;
    }

    /**
     * 获取用户信息
     *
     * @param accessToken
     * @return
     */
    public AlipayUserInfoShareResponse getUserInfo(String accessToken) {
        AlipayClient alipayClient = new DefaultAlipayClient(gatewayUrl,
                appId,
                privateKey,
                "json",
                alipayConfig.getCharset(),
                publicKey,
                alipayConfig.getSign_type());
        AlipayUserInfoShareRequest request = new AlipayUserInfoShareRequest();
        AlipayUserInfoShareResponse response = null;
        try {
            response = alipayClient.execute(request, accessToken);
        } catch (AlipayApiException e) {
            e.printStackTrace();
        }
        return response;
    }


    /**
     * 支付宝支付
     *
     * @param outTradeNo  商户订单号，商户网站订单系统中唯一订单号，必填   对应缴费记录的orderNo
     * @param totalAmount 付款金额，必填
     * @param subject     主题
     * @param body        商品描述，可空
     * @return
     */
    @Override
    public String alipay(String outTradeNo, String totalAmount, String subject, String body) {
        //获得初始化的AlipayClient
        AlipayClient alipayClient = new DefaultAlipayClient(alipayConfig.getGatewayUrl(),
                alipayConfig.getAppid(),
                alipayConfig.getPrivate_key(),
                "json",
                alipayConfig.getCharset(),
                alipayConfig.getAli_public_key(),
                alipayConfig.getSign_type());

        //设置请求参数
        AlipayTradePagePayRequest alipayRequest = new AlipayTradePagePayRequest();
        alipayRequest.setReturnUrl(alipayConfig.getReturn_url());
        try {
            alipayRequest.setBizContent("{\"out_trade_no\":\"" + outTradeNo + "\","
                    + "\"total_amount\":\"" + totalAmount + "\","
                    + "\"subject\":\"" + subject + "\","
                    + "\"body\":\"" + body + "\","
                    + "\"product_code\":\"FAST_INSTANT_TRADE_PAY\"}");

            //请求
            String result;

            result = alipayClient.pageExecute(alipayRequest).getBody();
            return result;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

}

