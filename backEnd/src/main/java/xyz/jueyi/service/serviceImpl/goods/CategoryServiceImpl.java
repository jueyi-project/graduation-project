package xyz.jueyi.service.serviceImpl.goods;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import xyz.jueyi.entity.goods.TbCategory;
import xyz.jueyi.mapper.goods.TbCategoryMapper;
import xyz.jueyi.service.goods.CategoryService;

import javax.annotation.Resource;
import java.util.List;

@Service
public class CategoryServiceImpl extends ServiceImpl<TbCategoryMapper, TbCategory> implements CategoryService {

    @Resource
    private TbCategoryMapper categoryMapper;


    @Override
    public List<TbCategory> getCategoriesByParentId(int parentId) {
        QueryWrapper<TbCategory> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("parent_id",parentId);
        return categoryMapper.selectList(queryWrapper);
    }
}
