package xyz.jueyi.service.serviceImpl.user;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import xyz.jueyi.entity.user.TbAreas;
import xyz.jueyi.mapper.user.TbAreasMapper;
import xyz.jueyi.service.user.AreasService;

import javax.annotation.Resource;
import java.util.List;

@Service
public class AreasServiceImpl extends ServiceImpl<TbAreasMapper, TbAreas> implements AreasService {

    @Resource
    private TbAreasMapper areasMapper;


    @Override
    public List<TbAreas> findAreasByCityId(String cityid) {
        QueryWrapper<TbAreas> queryWrapper = new QueryWrapper();
        return areasMapper.selectList(queryWrapper.eq("cityid", cityid));
    }
}
