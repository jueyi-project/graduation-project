package xyz.jueyi.service.serviceImpl.user;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import xyz.jueyi.entity.user.TbCities;
import xyz.jueyi.mapper.user.TbCitiesMapper;
import xyz.jueyi.service.user.CitiesService;

import javax.annotation.Resource;
import java.util.List;

@Service
public class CitiesServiceImpl extends ServiceImpl<TbCitiesMapper, TbCities> implements CitiesService {

    @Resource
    private TbCitiesMapper citiesMapper;


    @Override
    public List<TbCities> findCitiesByProvinceId(String provinceid) {
        QueryWrapper<TbCities> queryWrapper = new QueryWrapper();
        return citiesMapper.selectList(queryWrapper.eq("provinceid",provinceid));
    }
}
