package xyz.jueyi.service.serviceImpl;

import com.aliyun.oss.OSS;
import com.aliyun.oss.OSSClientBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import xyz.jueyi.config.AliyunOSSConfig;
import xyz.jueyi.methods.SystemDateUtils;
import xyz.jueyi.service.AliyunOssService;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;

@Service
public class AliyunOssServiceImpl implements AliyunOssService {

    @Autowired
    private AliyunOSSConfig aliyunOSSConfig;

    /**
     * OSS上传txt文本
     *
     * @return
     */
    @Override
    public String uploadTest() {
        String objectName = "test.txt";
        OSS ossClient = new OSSClientBuilder().build(
                aliyunOSSConfig.getEndpointName(),
                aliyunOSSConfig.getAccessKeyId(),
                aliyunOSSConfig.getAccessKeySecret());
        String content = SystemDateUtils.getStrDate() + ":Hello OSS";
        ossClient.putObject(aliyunOSSConfig.getBucketName(), objectName, new ByteArrayInputStream(content.getBytes()));
        ossClient.shutdown();
        String url = "https://" + aliyunOSSConfig.getBucketName() + "." + aliyunOSSConfig.getEndpointName() + '/' + objectName;
        return url;
    }

    /**
     * OSS上传图片
     *
     * @param file
     * @param fileName
     * @return
     * @throws IOException
     */
    private String uploadPhoto(MultipartFile file, String fileName) throws IOException {
        OSS ossClient = new OSSClientBuilder().build(
                aliyunOSSConfig.getEndpointName(),
                aliyunOSSConfig.getAccessKeyId(),
                aliyunOSSConfig.getAccessKeySecret());
        //获取上传文件流
        InputStream inputStream = file.getInputStream();
        //调用oss方法实现上传
        ossClient.putObject(aliyunOSSConfig.getBucketName(), fileName, inputStream);
        //关闭OSSClient
        ossClient.shutdown();
        //把上传之后文件路径返回
        //需要把上传到阿里云oss路径手动拼接起来,比如：https://jueyi-image.oss-cn-beijing.aliyuncs.com/1.jpg
        return "https://" + aliyunOSSConfig.getBucketName() + "." + aliyunOSSConfig.getEndpointName() + '/' + fileName;
    }

    @Override
    public String uploadAvatar(String username, MultipartFile file) {
        //获取文件名称
        String fileName = username + "_" + SystemDateUtils.getStrDate() + "_avatar.jpg";
        try {
            return uploadPhoto(file, fileName);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }


    @Override
    public String uploadCardFront(String username, MultipartFile file) {
        //获取文件名称
        String fileName = username + "_" + SystemDateUtils.getStrDate() + "_front.jpg";
        try {
            return uploadPhoto(file, fileName);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public String uploadCardBack(String username, MultipartFile file) {
        //获取文件名称
        String fileName = username + SystemDateUtils.getStrDate() + "_back.jpg";
        try {
            return uploadPhoto(file, fileName);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public String uploadAdPhoto(MultipartFile file) {
        //获取文件名称
        String fileName = SystemDateUtils.getStrDate() + "_banner.jpg";
        try {
            return uploadPhoto(file, fileName);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }


}


