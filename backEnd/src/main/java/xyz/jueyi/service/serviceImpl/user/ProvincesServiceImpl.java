package xyz.jueyi.service.serviceImpl.user;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import xyz.jueyi.entity.user.TbProvinces;
import xyz.jueyi.mapper.user.TbProvincesMapper;
import xyz.jueyi.service.user.ProvincesService;

import javax.annotation.Resource;
import java.util.List;

@Service
public class ProvincesServiceImpl extends ServiceImpl<TbProvincesMapper, TbProvinces> implements ProvincesService {

    @Resource
    private TbProvincesMapper provincesMapper;


    @Override
    public List<TbProvinces> findAll() {
        QueryWrapper<TbProvinces> queryWrapper = new QueryWrapper();
        return provincesMapper.selectList(queryWrapper);
    }
}
