package xyz.jueyi.service.serviceImpl.user;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import org.springframework.stereotype.Service;
import xyz.jueyi.entity.PageResult;
import xyz.jueyi.entity.user.TbUser;
import xyz.jueyi.mapper.user.TbUserMapper;
import xyz.jueyi.methods.SystemDateUtils;
import xyz.jueyi.service.user.UserService;

import javax.annotation.Resource;
import java.util.List;

@Service
public class UserServiceImpl extends ServiceImpl<TbUserMapper, TbUser> implements UserService {

    @Resource
    private TbUserMapper tbUserMapper;

    @Override
    public TbUser findById(String username) {
        return tbUserMapper.selectById(username);
    }

    @Override
    public List<TbUser> findAll() {
        QueryWrapper<TbUser> queryWrapper = new QueryWrapper<>();
        List<TbUser> userList = tbUserMapper.selectList(queryWrapper);
        userList.forEach(item -> {
            item.setPassword(null);
        });
        return userList;
    }

    @Override
    public List<TbUser> findByPhone(String phone) {
        QueryWrapper<TbUser> queryWrapper = new QueryWrapper();
        List<TbUser> userList = tbUserMapper.selectList(queryWrapper.eq("phone", phone));
        return userList;
    }

    @Override
    public void add(TbUser user) {
        tbUserMapper.insert(user);
    }

    @Override
    public void updateName(String name, String username) {
        TbUser user = tbUserMapper.selectById(username);
        user.setName(name);
        user.setUpdated(SystemDateUtils.getDate());
        tbUserMapper.updateById(user);
    }

    @Override
    public void updateHeadPic(String headPic, String username) {
        TbUser user = tbUserMapper.selectById(username);
        user.setHeadPic(headPic);
        user.setUpdated(SystemDateUtils.getDate());
        tbUserMapper.updateById(user);
    }

    @Override
    public void updatePhone(String phone, String username) {
        TbUser user = tbUserMapper.selectById(username);
        user.setPhone(phone);
        user.setUpdated(SystemDateUtils.getDate());
        tbUserMapper.updateById(user);
    }

    @Override
    public void updatePsw(String password, String username) {
        TbUser user = tbUserMapper.selectById(username);
        user.setPassword(password);
        user.setUpdated(SystemDateUtils.getDate());
        tbUserMapper.updateById(user);
    }

}
