package xyz.jueyi.service.serviceImpl;

import com.baidu.aip.ocr.AipOcr;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.stereotype.Service;
import xyz.jueyi.config.BaiduApiConfig;
import xyz.jueyi.service.BaiduApiService;

import javax.annotation.Resource;
import java.util.HashMap;


@Service
public class BaiduApiServiceImpl implements BaiduApiService {


    @Resource
    private BaiduApiConfig baiduApiConfig;


    @Override
    public JSONObject getInfo(byte[] file, String idCardSide) {

        // 初始化一个AipOcr
        AipOcr client = new AipOcr(baiduApiConfig.getApp_id(), baiduApiConfig.getApi_key(), baiduApiConfig.getSecret_key());

        // 可选：设置网络连接参数
        client.setConnectionTimeoutInMillis(2000);
        client.setSocketTimeoutInMillis(60000);

        // 传入可选参数调用接口
        HashMap<String, String> options = new HashMap<String, String>();
        // 是否检测朝向
        options.put("detect_direction", "false");
        // 是否检测风险
        options.put("detect_risk", "false");

        try {
            JSONObject res = client.idcard(file, idCardSide, options);

            if (res.getString("image_status").equals("normal") && idCardSide.equals("front")) {
                JSONObject idCard = new JSONObject();
                JSONObject words_result = res.getJSONObject("words_result");
                idCard.put("name", words_result.getJSONObject("姓名").get("words"));
                idCard.put("nation", words_result.getJSONObject("民族").get("words"));
                idCard.put("address", words_result.getJSONObject("住址").get("words"));
                idCard.put("sex", words_result.getJSONObject("性别").get("words"));
                idCard.put("birth", words_result.getJSONObject("出生").get("words"));
                idCard.put("number", words_result.getJSONObject("公民身份号码").get("words"));
                return idCard;
            } else if (res.getString("image_status").equals("normal") && idCardSide.equals("back")) {
                JSONObject idCard = new JSONObject();
                JSONObject words_result = res.getJSONObject("words_result");
                idCard.put("expirationDate", words_result.getJSONObject("失效日期").get("words"));
                idCard.put("dateOfIssue", words_result.getJSONObject("签发日期").get("words"));
                idCard.put("issuingAuthority", words_result.getJSONObject("签发机关").get("words"));
                return idCard;
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

}

