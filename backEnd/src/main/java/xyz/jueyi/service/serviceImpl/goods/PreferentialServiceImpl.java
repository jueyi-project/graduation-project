package xyz.jueyi.service.serviceImpl.goods;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import org.springframework.stereotype.Service;
import xyz.jueyi.entity.PageResult;
import xyz.jueyi.entity.goods.TbCategory;
import xyz.jueyi.entity.goods.TbPreferential;
import xyz.jueyi.mapper.goods.TbCategoryMapper;
import xyz.jueyi.mapper.goods.TbPreferentialMapper;
import xyz.jueyi.methods.SystemDateUtils;
import xyz.jueyi.service.goods.PreferentialService;

import javax.annotation.Resource;
import java.util.List;

@Service
public class PreferentialServiceImpl extends ServiceImpl<TbPreferentialMapper, TbPreferential> implements PreferentialService {

    @Resource
    private TbPreferentialMapper preferentialMapper;

    @Resource
    private TbCategoryMapper categoryMapper;

    @Override
    public int findPreMoneyByCategoryId(Integer categoryId, int money) {
        QueryWrapper<TbPreferential> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("state", "1");   //状态
        queryWrapper.eq("category_id", categoryId);  //分类
        queryWrapper.le("buy_money", money);//消费额
        queryWrapper.ge("end_time", SystemDateUtils.getDate()); //截至日期大于等于当前日期
        queryWrapper.le("start_time", SystemDateUtils.getDate());// 开始日期小于等于当前日期
        queryWrapper.orderByDesc("buy_money");//按购买金额降序排序


        List<TbPreferential> preferentials = preferentialMapper.selectList(queryWrapper);

        if (preferentials.size() >= 1) {//有优惠
            TbPreferential preferential = preferentials.get(0);
            if ("1".equals(preferential.getType())) { //如果不翻倍
                return preferential.getPreMoney();
            } else {
                //计算倍数
                int multiple = money / preferential.getBuyMoney();
                return preferential.getPreMoney() * multiple;
            }

        } else {//没有优惠
            return 0;
        }

    }

    @Override
    public PageResult<TbPreferential> findPage(int page, int size) {
        PageHelper.startPage(page, size);
        QueryWrapper<TbPreferential> queryWrapper = new QueryWrapper<>();
        List<TbPreferential> preferentials = preferentialMapper.selectList(queryWrapper);
        preferentials.forEach(preferential -> {
            TbCategory category3 = categoryMapper.selectById(preferential.getCategoryId());
            TbCategory category2 = categoryMapper.selectById(category3.getParentId());
            TbCategory category1 = categoryMapper.selectById(category2.getParentId());
            preferential.setCategoryName(category1.getName() + "/" + category2.getName() + "/" + category3.getName());
        });
        Page<TbPreferential> pages = (Page<TbPreferential>) preferentials;
        return new PageResult<>(pages.getTotal(), pages.getResult());
    }

    @Override
    public TbPreferential getPreferentialByCategoryId(int categoryId) {
        QueryWrapper<TbPreferential> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("category_id", categoryId);

        if (preferentialMapper.selectOne(queryWrapper) != null) {
            TbPreferential preferential = preferentialMapper.selectOne(queryWrapper);
            TbCategory category3 = categoryMapper.selectById(preferential.getCategoryId());
            TbCategory category2 = categoryMapper.selectById(category3.getParentId());
            TbCategory category1 = categoryMapper.selectById(category2.getParentId());
            preferential.setCategoryName(category1.getName() + "/" + category2.getName() + "/" + category3.getName());
            return preferential;
        } else return null;
    }

    @Override
    public TbPreferential findByCategoryId(int categoryId) {
        QueryWrapper<TbPreferential> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("category_id", categoryId);
        queryWrapper.eq("state", "1");
        queryWrapper.lt("start_time", SystemDateUtils.getDate());
        queryWrapper.gt("end_time", SystemDateUtils.getDate());

        return preferentialMapper.selectOne(queryWrapper);
    }

}
