package xyz.jueyi.service.serviceImpl.order;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import xyz.jueyi.entity.IdWorker;
import xyz.jueyi.entity.PageResult;
import xyz.jueyi.entity.goods.TbSku;
import xyz.jueyi.entity.goods.TbSpu;
import xyz.jueyi.entity.order.*;
import xyz.jueyi.mapper.goods.TbCategoryMapper;
import xyz.jueyi.mapper.order.TbOrderConfigMapper;
import xyz.jueyi.mapper.order.TbOrderItemMapper;
import xyz.jueyi.mapper.order.TbOrderLogMapper;
import xyz.jueyi.mapper.order.TbOrderMapper;
import xyz.jueyi.methods.SystemDateUtils;
import xyz.jueyi.service.goods.SkuService;
import xyz.jueyi.service.goods.SpuService;
import xyz.jueyi.service.order.OrderService;
import xyz.jueyi.service.user.CartService;

import javax.annotation.Resource;
import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

@Service
public class OrderServiceImpl extends ServiceImpl<TbOrderMapper, TbOrder> implements OrderService {

    @Resource
    private TbOrderMapper orderMapper;

    @Resource
    private TbOrderItemMapper orderItemMapper;

    @Resource
    private TbOrderLogMapper orderLogMapper;

    @Resource
    private TbOrderConfigMapper orderConfigMapper;

    @Resource
    private TbCategoryMapper categoryMapper;

    @Resource
    private IdWorker idWorker;

    @Autowired
    private CartService cartService;

    @Autowired
    private SkuService skuService;

    @Autowired
    private SpuService spuService;


    /**
     * 普通订单新增方法,从购物车下单
     *
     * @param order
     * @return
     */
    @Override
    public Map<String, Object> add(TbOrder order) {
        //1获取购物车商品价格
        List<Map<String, Object>> orderItemList = cartService.findNewOrderItemList(order.getUsername());
        //2获取选中的购物车商品
        List<TbOrderItem> orderItems = orderItemList.stream()
                .filter(cart -> (boolean) cart.get("checked"))
                .map(cart -> (TbOrderItem) cart.get("item"))
                .collect(Collectors.toList());
        //3判断扣减库存
        if (!skuService.deductionStock(orderItems)) {
            throw new RuntimeException("库存扣减失败");
        }
        //4保存订单主表数据
        order.setId(idWorker.nextId() + "");
        //合计娄计算
        IntStream numStream = orderItems.stream().mapToInt(TbOrderItem::getNum);
        IntStream moneyStream = orderItems.stream().mapToInt(TbOrderItem::getMoney);
        int totalNum = numStream.sum();//总数据
        int totalMoney = moneyStream.sum();//订单总金额
        //计算优惠金额
        int preMoney = cartService.preferential(order.getUsername());
        order.setOrderStatus("0");  //创建订单
        order.setTotalNum(totalNum);  //总数量
        order.setTotalMoney(totalMoney);   //总金额
        order.setPreMoney(preMoney);   //优惠金额
        order.setPayMoney(totalMoney - preMoney);   //支付金额
        order.setCreateTime(SystemDateUtils.getDate());   //订单创建时间
        order.setPayStatus("0");//支付状态为未支付
        order.setWaitSend(0);  //待发货数
        if (order.getPayType().equals("0")) order.setWaitSend(orderItemList.size()); //选择货到付款，待发货数为items的数量
        order.setWaitReceive(0); //待收货数
        order.setWaitReview(0); //待评论数
        order.setIsDelete("0");//不是删除订单
        order.setIsGroup("0");//不是拼团订单
        orderMapper.insert(order);

        //5保存订单的明细表数据
        for (TbOrderItem orderItem : orderItems) {
            orderItem.setOrderId(order.getId());
            orderItem.setId(idWorker.nextId() + "");
            orderItem.setPostFee(0);  //正常应该根据收货地址设计运费
            //未发货
            orderItem.setConsignStatus("0");
            //未收货
            orderItem.setReceiveStatus("0");
            //未评论
            orderItem.setRateStatus("0");
            //不是退款订单
            orderItem.setIsReturn("0");
            orderItemMapper.insert(orderItem);
        }
        //6清除购车里选中的商品
        cartService.deleteCheckedCart(order.getUsername());

        //7记录
        TbOrderLog orderLog = new TbOrderLog();
        orderLog.setId(idWorker.nextId() + "");
        orderLog.setOrderId(order.getId());
        orderLog.setOrderStatus(order.getOrderStatus());
        orderLog.setConsignStatus("0");
        orderLog.setOperateTime(order.getCreateTime());
        orderLog.setOperater("user:" + order.getUsername());   //谁操作的
        orderLog.setPayStatus(order.getPayStatus());
        orderLog.setRemarks("创建订单");
        orderLogMapper.insert(orderLog);

        //8返回订单号和支付金额
        Map<String, Object> map = new HashMap<>();
        map.put("ordersn", order.getId());
        map.put("money", order.getPayMoney());

        return map;

    }


    /**
     * 立即购买下单
     *
     * @param order
     * @param orderItem
     * @return
     */
    @Override
    public Map<String, Object> add(TbOrder order, TbOrderItem orderItem) {

        List<TbOrderItem> orderItems = new ArrayList<>();
        orderItems.add(orderItem);
        //1判断扣减库存
        if (!skuService.deductionStock(orderItems)) {
            throw new RuntimeException("库存扣减失败");
        }

        //2保存订单主表数据
        order.setId(idWorker.nextId() + "");
        order.setOrderStatus("0");  //创建订单
        order.setCreateTime(SystemDateUtils.getDate());   //订单创建时间
        order.setPayStatus("0");//支付状态为未支付
        order.setWaitSend(0);  //待发货数
        if (order.getPayType().equals("0")) order.setWaitSend(1); //选择货到付款，待发货数为items的数量
        order.setWaitReceive(0); //待收货数
        order.setWaitReview(0); //待评论数
        order.setIsDelete("0");//不是删除订单
        order.setIsGroup("0");//不是拼团订单
        orderMapper.insert(order);

        //3保存订单的明细表数据
        orderItem.setOrderId(order.getId());
        orderItem.setId(idWorker.nextId() + "");
        orderItem.setCategoryId2(categoryMapper.selectById(orderItem.getCategoryId3()).getParentId());
        orderItem.setCategoryId1(categoryMapper.selectById(orderItem.getCategoryId2()).getParentId());
        //未发货
        orderItem.setConsignStatus("0");
        //未收货
        orderItem.setReceiveStatus("0");
        //未评论
        orderItem.setRateStatus("0");
        //不是退款订单
        orderItem.setIsReturn("0");
        orderItemMapper.insert(orderItem);


        //4记录
        TbOrderLog orderLog = new TbOrderLog();
        orderLog.setId(idWorker.nextId() + "");
        orderLog.setOrderId(order.getId());
        orderLog.setOrderStatus(order.getOrderStatus());
        orderLog.setConsignStatus("0");
        orderLog.setOperateTime(order.getCreateTime());
        orderLog.setOperater("user:" + order.getUsername());   //谁操作的
        orderLog.setPayStatus(order.getPayStatus());
        orderLog.setRemarks("创建订单");
        orderLogMapper.insert(orderLog);

        //5返回订单号和支付金额
        Map<String, Object> map = new HashMap<>();
        map.put("ordersn", order.getId());
        map.put("money", order.getPayMoney());

        return map;
    }


    /**
     * 普通线上支付的订单，支付超时自动关闭
     *
     * @return
     */
    @Override
    @Transactional
    public int orderTimeOutLogic() {
        //订单超时未付款，自动关闭
        TbOrderConfig orderConfig = orderConfigMapper.selectById(1);
        int orderTimeOut = orderConfig.getOrderTimeout();

        LocalDateTime localDateTime = LocalDateTime.now().minusMinutes(orderTimeOut);   //当前时间 - orderTimeOut

        QueryWrapper<TbOrder> queryWrapper = new QueryWrapper();
        queryWrapper.eq("pay_status", "0");     //未支付的
        queryWrapper.eq("pay_type", "1");     //线上支付的
        queryWrapper.eq("is_delete", "0");    //不是删除的
        queryWrapper.le("create_time", localDateTime);  //createTime + orderTimeOut > 当前时间 ，则不用关闭
        queryWrapper.eq("order_status", "0");
        List<TbOrder> orders = orderMapper.selectList(queryWrapper);
        return closeOrder(orders);

    }

    /**
     * 关闭普通订单
     *
     * @param orders
     * @return
     */
    private int closeOrder(List<TbOrder> orders) {
        orders.forEach(order -> {
            Date date = SystemDateUtils.getDate();
            TbOrderLog orderLog = new TbOrderLog();
            orderLog.setId(idWorker.nextId() + "");
            orderLog.setOrderId(order.getId());
            orderLog.setOrderStatus("1");
            orderLog.setOperateTime(date);
            orderLog.setOperater("system");   //谁操作的
            orderLog.setPayStatus(order.getPayStatus());
            orderLog.setRemarks("超时未付款，订单关闭");
            orderLogMapper.insert(orderLog);

            order.setOrderStatus("1");
            order.setUpdateTime(date);
            order.setCloseTime(date);
            order.setIsDelete("1");

            //订单关闭，恢复spu和sku的销量，sku的库存
            QueryWrapper<TbOrderItem> queryWrapper = new QueryWrapper();
            queryWrapper.eq("order_id", order.getId());
            List<TbOrderItem> orderItemList = orderItemMapper.selectList(queryWrapper);
            for (TbOrderItem orderItem : orderItemList) {
                //spu销量
                TbSpu spu = spuService.findById(orderItem.getSpuId());
                spu.setSaleNum(spu.getSaleNum() - orderItem.getNum());
                spuService.updateById(spu);
                //sku销量
                TbSku sku = skuService.findById(orderItem.getSkuId());
                sku.setSaleNum(sku.getSaleNum() - orderItem.getNum());
                //sku库存
                sku.setNum(sku.getNum() + orderItem.getNum());
                skuService.updateById(sku);
            }
            orderMapper.updateById(order);
        });

        return orders.size();
    }

    /**
     * 用户取消订单
     *
     * @param username
     * @param orderId
     */
    @Override
    public void closeOrder(String username, String orderId) {
        Date date = SystemDateUtils.getDate();
        TbOrder order = orderMapper.selectById(orderId);

        TbOrderLog orderLog = new TbOrderLog();
        orderLog.setId(idWorker.nextId() + "");
        orderLog.setOrderId(order.getId());
        orderLog.setOrderStatus("1");
        orderLog.setOperateTime(date);
        orderLog.setOperater("user:" + username);   //谁操作的
        orderLog.setPayStatus(order.getPayStatus());
        orderLog.setRemarks("用户取消了订单");
        orderLogMapper.insert(orderLog);

        order.setOrderStatus("1");
        order.setUpdateTime(date);
        order.setCloseTime(date);
        order.setIsDelete("1");

        //订单关闭，恢复spu和sku的销量，sku的库存
        QueryWrapper<TbOrderItem> queryWrapper = new QueryWrapper();
        queryWrapper.eq("order_id", order.getId());
        List<TbOrderItem> orderItemList = orderItemMapper.selectList(queryWrapper);
        for (TbOrderItem orderItem : orderItemList) {
            //spu销量
            TbSpu spu = spuService.findById(orderItem.getSpuId());
            spu.setSaleNum(spu.getSaleNum() - orderItem.getNum());
            spuService.updateById(spu);
            //sku销量
            TbSku sku = skuService.findById(orderItem.getSkuId());
            sku.setSaleNum(sku.getSaleNum() - orderItem.getNum());
            //sku库存
            sku.setNum(sku.getNum() + orderItem.getNum());
            skuService.updateById(sku);
        }
        orderMapper.updateById(order);
    }

    /**
     * 用户删除完成的订单
     *
     * @param username
     * @param orderId
     */
    @Override
    public void delOrder(String username, String orderId) {
        Date date = SystemDateUtils.getDate();
        TbOrder order = orderMapper.selectById(orderId);

        TbOrderLog orderLog = new TbOrderLog();
        orderLog.setId(idWorker.nextId() + "");
        orderLog.setOrderId(order.getId());
        orderLog.setOrderStatus("1");
        orderLog.setOperateTime(date);
        orderLog.setOperater("user:" + username);   //谁操作的
        orderLog.setPayStatus(order.getPayStatus());
        orderLog.setRemarks("用户删除了订单");
        orderLogMapper.insert(orderLog);

        order.setUpdateTime(date);
        order.setCloseTime(date);
        order.setIsDelete("1");

        orderMapper.updateById(order);
    }

    /**
     * 支付后，更新订单状态
     *
     * @param orderId
     */
    @Override
    public void updatePayOrder(String orderId, String transactionId) {
        Date date = SystemDateUtils.getDate();
        QueryWrapper<TbOrderItem> queryWrapper = new QueryWrapper();
        queryWrapper.eq("order_id", orderId);
        List<TbOrderItem> orderItems = orderItemMapper.selectList(queryWrapper);
        TbOrder order = orderMapper.selectById(orderId);
        order.setUpdateTime(date);
        order.setPayTime(date);
        order.setPayStatus("1");
        order.setWaitSend(orderItems.size());
        order.setTransactionId(transactionId);
        orderMapper.updateById(order);
        //记录
        TbOrderLog orderLog = new TbOrderLog();
        orderLog.setId(idWorker.nextId() + "");
        orderLog.setOrderId(order.getId());
        orderLog.setOrderStatus("0");
        orderLog.setConsignStatus("0");
        orderLog.setOperateTime(date);
        orderLog.setOperater("user:" + order.getUsername());   //谁操作的
        orderLog.setPayStatus("1");
        orderLog.setRemarks("用户在线支付了订单");
        orderLogMapper.insert(orderLog);
    }

    /**
     * 根据username查出未删除的order
     *
     * @param username
     * @return
     */
    @Override
    public PageResult<TbOrder> findOrdersNotDel(String username, int page, int size) {
        PageHelper.startPage(page, size);
        QueryWrapper<TbOrder> queryWrapper = new QueryWrapper();
        queryWrapper.eq("username", username);
        queryWrapper.eq("is_delete", "0");
        //降序排序
        queryWrapper.orderByDesc("create_time");
        Page<TbOrder> orders = (Page<TbOrder>) orderMapper.selectList(queryWrapper);
        return new PageResult<TbOrder>(orders.getTotal(), orders.getResult());
    }

    /**
     * 根据订单状态查找订单，0-待付款，1-待发货，2-待收货，3-待评价
     *
     * @param page
     * @param size
     * @return
     */
    @Override
    public PageResult<TbOrder> findOrdersByOrderStatus(String username, int page, int size, String orderStatus) {
        PageHelper.startPage(page, size);
        QueryWrapper<TbOrder> queryWrapper = new QueryWrapper();
        queryWrapper.eq("username", username);
        //未关闭
        queryWrapper.eq("order_status", "0");
        //未删除
        queryWrapper.eq("is_delete", "0");
        if (orderStatus.equals("0")) {
            //线上支付
            queryWrapper.eq("pay_type", "1");
            //未付款
            queryWrapper.eq("pay_status", "0");
        } else if (orderStatus.equals("1")) {
            //待发货数大于0
            queryWrapper.gt("wait_send", 0);
        } else if (orderStatus.equals("2")) {
            //待收货数大于0
            queryWrapper.gt("wait_receive", 0);
        } else {
            //待评价数大于0
            queryWrapper.gt("wait_review", 0);
        }

        queryWrapper.orderByDesc("create_time");
        Page<TbOrder> orders = (Page<TbOrder>) orderMapper.selectList(queryWrapper);
        return new PageResult<TbOrder>(orders.getTotal(), orders.getResult());
    }


    /**
     * 根据orderList返回orderAndItemsList
     *
     * @param orderStatus
     * @param orderList
     * @return
     */
    @Override
    public List<OrderAndItems> findOrderAndItems(String orderStatus, List<TbOrder> orderList) {

        List<OrderAndItems> orderAndItemsList = new ArrayList<>();
        for (TbOrder order : orderList) {
            OrderAndItems orderAndItems = new OrderAndItems();
            QueryWrapper<TbOrderItem> queryWrapper = new QueryWrapper();
            queryWrapper.eq("order_id", order.getId());
            if (orderStatus.equals("1")) {
                queryWrapper.eq("consign_status", "0");
            } else if (orderStatus.equals("2")) {
                queryWrapper.eq("receive_status", "0");
            } else if (orderStatus.equals("3")) {
                queryWrapper.eq("rate_status", "0");
            }
            List<TbOrderItem> orderItemList = orderItemMapper.selectList(queryWrapper);
            orderAndItems.setOrder(order);
            orderAndItems.setOrderItemList(orderItemList);
            orderAndItemsList.add(orderAndItems);
        }
        return orderAndItemsList;
    }
}
