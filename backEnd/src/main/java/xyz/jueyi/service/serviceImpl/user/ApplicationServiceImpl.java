package xyz.jueyi.service.serviceImpl.user;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import org.springframework.stereotype.Service;
import xyz.jueyi.entity.PageResult;
import xyz.jueyi.entity.user.TbApplication;
import xyz.jueyi.mapper.user.TbApplicationMapper;
import xyz.jueyi.service.user.ApplicationService;

import javax.annotation.Resource;
import java.util.List;

@Service
public class ApplicationServiceImpl extends ServiceImpl<TbApplicationMapper, TbApplication> implements ApplicationService {

    @Resource
    private TbApplicationMapper applicationMapper;


    @Override
    public TbApplication findApplicationByState(String username, String state) {
        QueryWrapper<TbApplication> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("username", username);
        queryWrapper.eq("state", state);
        return applicationMapper.selectOne(queryWrapper);
    }

    @Override
    public PageResult<TbApplication> getNotReviewed(int page, int size) {
        PageHelper.startPage(page, size);
        QueryWrapper<TbApplication> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("state", "0");
        queryWrapper.orderByAsc("apply_time");
        Page<TbApplication> pages = (Page<TbApplication>) applicationMapper.selectList(queryWrapper);
        return new PageResult<>(pages.getTotal(), pages.getResult());
    }

    @Override
    public PageResult<TbApplication> getReviewed(int page,int size) {
        PageHelper.startPage(page, size);
        QueryWrapper<TbApplication> queryWrapper = new QueryWrapper<>();
        queryWrapper.ne("state", "0");
        queryWrapper.orderByDesc("opreate_time");
        Page<TbApplication> pages = (Page<TbApplication>) applicationMapper.selectList(queryWrapper);
        return new PageResult<>(pages.getTotal(), pages.getResult());
    }
}
