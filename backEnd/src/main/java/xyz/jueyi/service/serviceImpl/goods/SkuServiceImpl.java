package xyz.jueyi.service.serviceImpl.goods;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import xyz.jueyi.entity.PageResult;
import xyz.jueyi.entity.goods.TbSku;
import xyz.jueyi.entity.order.TbOrderItem;
import xyz.jueyi.mapper.goods.TbSkuMapper;
import xyz.jueyi.service.goods.SkuService;

import javax.annotation.Resource;
import java.util.List;


@Service
public class SkuServiceImpl extends ServiceImpl<TbSkuMapper, TbSku> implements SkuService {

    @Resource
    private TbSkuMapper skuMapper;


    private List<TbSku> selectAll() {
        QueryWrapper<TbSku> queryWrapper = new QueryWrapper<>();
        return skuMapper.selectList(queryWrapper);
    }


    @Override
    public PageResult<TbSku> findPage(int page, int size) {
        PageHelper.startPage(page, size);
        Page<TbSku> skus = (Page<TbSku>) selectAll();
        return new PageResult<TbSku>(skus.getTotal(), skus.getResult());
    }

    @Override
    public TbSku findById(String id) {
        return skuMapper.selectById(id);
    }

    @Override
    public List<TbSku> search(String str) {
        return skuMapper.search(str);
    }

    @Override
    public PageResult<TbSku> searchPage(String str, int page, int size) {
        PageHelper.startPage(page, size);
        Page<TbSku> skus = (Page<TbSku>) search(str);
        return new PageResult<TbSku>(skus.getTotal(), skus.getResult());
    }

    @Override
    @Transactional
    public boolean deductionStock(List<TbOrderItem> orderItemList) {
        //检查是否可以扣减库存
        boolean idDeduction = true;//是否可以扣减
        for (TbOrderItem orderItem : orderItemList) {
            TbSku sku = findById(orderItem.getSkuId());
            if (sku == null) {
                idDeduction = false;
                break;
            }
            if (!"1".equals(sku.getStatus())) {
                idDeduction = false;
                break;
            }
            if (sku.getNum() < orderItem.getNum()) {
                //TODO:这个地方需要提示信息
                idDeduction = false;
                break;
            }
        }
        //执行扣减方法
        if (idDeduction) {
            for (TbOrderItem orderItem : orderItemList) {
                //执行扣减库存方法
                skuMapper.deductionstock(orderItem.getSkuId(), orderItem.getNum());
                //TODO：扣减完需要判断库存是不是为0了，如果是，要下架商品
                //执行增加销量方法
                skuMapper.addSaleNum(orderItem.getSkuId(), orderItem.getNum());
            }
        }
        return idDeduction;
    }
}
