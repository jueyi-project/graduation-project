package xyz.jueyi.service.serviceImpl.user;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import xyz.jueyi.entity.user.TbAddress;
import xyz.jueyi.mapper.user.TbAddressMapper;
import xyz.jueyi.service.user.AddressService;

import javax.annotation.Resource;
import java.util.List;

@Service
public class AddressServiceImpl extends ServiceImpl<TbAddressMapper, TbAddress> implements AddressService {

    @Resource
    private TbAddressMapper addressMapper;


    @Override
    public List<TbAddress> findAddressByUsername(String username) {
        QueryWrapper<TbAddress> queryWrapper = new QueryWrapper();
        return addressMapper.selectList(queryWrapper.eq("username", username));
    }

    @Override
    public TbAddress findByUsernameAndDefault(String username) {
        QueryWrapper<TbAddress> queryWrapper = new QueryWrapper();
        return addressMapper.selectOne(queryWrapper.eq("username", username).eq("is_default", "1"));
    }

}
