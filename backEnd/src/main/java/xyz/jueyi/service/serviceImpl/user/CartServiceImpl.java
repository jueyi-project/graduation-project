package xyz.jueyi.service.serviceImpl.user;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;
import xyz.jueyi.entity.goods.TbCategory;
import xyz.jueyi.entity.goods.TbSku;
import xyz.jueyi.entity.order.TbOrderItem;
import xyz.jueyi.methods.CacheKey;
import xyz.jueyi.methods.SystemDateUtils;
import xyz.jueyi.service.goods.CategoryService;
import xyz.jueyi.service.goods.PreferentialService;
import xyz.jueyi.service.goods.SkuService;
import xyz.jueyi.service.user.CartService;

import java.util.*;
import java.util.stream.Collectors;

@Service
public class CartServiceImpl implements CartService {

    @Autowired
    private RedisTemplate redisTemplate;

    @Autowired
    private SkuService skuService;

    @Autowired
    private CategoryService categoryService;

    @Autowired
    private PreferentialService preferentialService;


    @Override
    public List<Map<String, Object>> findCartList(String username) {

        List<Map<String, Object>> cartList = (List<Map<String, Object>>) redisTemplate.boundHashOps(CacheKey.CAST_LIST).get(username);
        if (cartList == null) {
            cartList = new ArrayList<>();
        }
        return cartList;
    }

    @Override
    public void addGoods(String username, String skuId, Integer num) {
        //实现思路：遍历购物车，如果购物车中存在该商品则数量累加，如果不存在则添加购物车项
        //获取购物车
        List<Map<String, Object>> cartList = findCartList(username);
        boolean flag = false;//是否在购物车中存在
        for (Map map : cartList) {
            TbOrderItem orderItem = (TbOrderItem) map.get("item");
            //购物车存在该商品
            if (orderItem.getSkuId().equals(skuId)) {
                //单个商品的重量
                int weight = orderItem.getWeight() / orderItem.getNum();
                //已有的话就加上原来的
                orderItem.setNum(orderItem.getNum() + num);
                //总金额计算
                orderItem.setMoney(orderItem.getNum() * orderItem.getPrice());
                //支付金额计算
                int preMoney = preferentialService.findPreMoneyByCategoryId(orderItem.getCategoryId3(), orderItem.getMoney());
                System.out.println(preMoney);
                orderItem.setPayMoney(orderItem.getMoney() - preMoney);
                //重量计算
                orderItem.setWeight(weight * orderItem.getNum());
                if (orderItem.getNum() <= 0) {
                    //购物车项删除
                    cartList.remove(map);
                }
                flag = true;
                break;
            }
        }
        //购物车不存在该商品
        if (flag == false) {
            TbSku sku = skuService.findById(skuId);
            if (sku == null) {
                throw new RuntimeException("商品不存在");
            }
            if (!"1".equals(sku.getStatus())) {
                throw new RuntimeException("商品的状态不合法");
            }
            //数量不能为0或负数
            if (num <= 0) {
                throw new RuntimeException("商品的数量不合法");
            }
            TbOrderItem orderItem = new TbOrderItem();
            orderItem.setName(sku.getName());
            orderItem.setSkuId(skuId);
            orderItem.setSpuId(sku.getSpuId());
            orderItem.setNum(num);
            orderItem.setImage(sku.getImage());
            orderItem.setPrice(sku.getPrice());
            //商品分类
            orderItem.setCategoryId3(sku.getCategoryId());
            TbCategory category3 = (TbCategory) redisTemplate.boundHashOps(CacheKey.CATEGORY).get(sku.getCategoryId());
            if (category3 == null) {
                category3 = categoryService.getById(sku.getCategoryId());//根据3级分类id查2级
                redisTemplate.boundHashOps(CacheKey.CATEGORY).put(sku.getCategoryId(), category3);
            }
            orderItem.setCategoryId2(category3.getParentId());
            TbCategory category2 = (TbCategory) redisTemplate.boundHashOps(CacheKey.CATEGORY).get(category3.getParentId());
            if (category2 == null) {
                category2 = categoryService.getById(category3.getParentId());//根据2级分类id查询1级
                redisTemplate.boundHashOps(CacheKey.CATEGORY).put(category3.getParentId(), category2);
            }
            orderItem.setCategoryId1(category2.getParentId());
            //默认不开发票
            orderItem.setInvoice("0");
            //总金额计算
            orderItem.setMoney(orderItem.getPrice() * num);
            //支付金额计算
            int preMoney = preferentialService.findPreMoneyByCategoryId(orderItem.getCategoryId3(), orderItem.getMoney());
            orderItem.setPayMoney(orderItem.getMoney() - preMoney);
            //重量计算
            orderItem.setWeight(sku.getWeight() * num);

            Map map = new HashMap();
            map.put("item", orderItem);
            map.put("checked", true);//默认选中

            cartList.add(map);
        }
        redisTemplate.boundHashOps(CacheKey.CAST_LIST).put(username, cartList);

    }

    @Override
    public void updateNum(String username, String skuId, Integer num) {
        //获取购物车
        List<Map<String, Object>> cartList = findCartList(username);
        for (Map map : cartList) {
            TbOrderItem orderItem = (TbOrderItem) map.get("item");
            if (orderItem.getSkuId().equals(skuId)) {//购物车存在该商品
                if (orderItem.getNum() <= 0) {
                    cartList.remove(map);//购物车项删除
                    break;
                }
                int weight = orderItem.getWeight() / orderItem.getNum();//单个商品的重量
                orderItem.setNum(num);  //设置数量
                orderItem.setMoney(orderItem.getNum() * orderItem.getPrice());//金额计算
                int preMoney = preferentialService.findPreMoneyByCategoryId(orderItem.getCategoryId3(), orderItem.getMoney());
                orderItem.setPayMoney(orderItem.getMoney() - preMoney);
                orderItem.setWeight(weight * orderItem.getNum());//重量计算
                if (orderItem.getNum() <= 0) {
                    cartList.remove(map);//购物车项删除
                }
                break;
            }
        }
        redisTemplate.boundHashOps(CacheKey.CAST_LIST).put(username, cartList);
    }

    @Override
    public void updateInvoice(String username, String skuId) {
        //获取购物车
        List<Map<String, Object>> cartList = findCartList(username);
        for (Map map : cartList) {
            TbOrderItem orderItem = (TbOrderItem) map.get("item");
            if (orderItem.getSkuId().equals(skuId)) {//购物车存在该商品
                if (orderItem.getInvoice().equals("0")) orderItem.setInvoice("1");
                else orderItem.setInvoice("0");
                break;
            }
        }
        redisTemplate.boundHashOps(CacheKey.CAST_LIST).put(username, cartList);
    }

    @Override
    public void updateBuyerMessage(String username, String skuId, String msg) {
        //获取购物车
        List<Map<String, Object>> cartList = findCartList(username);
        for (Map map : cartList) {
            TbOrderItem orderItem = (TbOrderItem) map.get("item");
            if (orderItem.getSkuId().equals(skuId)) {//购物车存在该商品
                orderItem.setBuyerMessage(msg);
                break;
            }
        }
        redisTemplate.boundHashOps(CacheKey.CAST_LIST).put(username, cartList);
    }


    @Override
    public void updateCheckedByList(String username, List<String> skuList) {
        //获取购物车
        List<Map<String, Object>> cartList = findCartList(username);

        for (Map map : cartList) {
            TbOrderItem orderItem = (TbOrderItem) map.get("item");
            if (skuList.contains(orderItem.getSkuId())) {
                map.put("checked", true);
            } else {
                map.put("checked", false);
            }
        }
        //存入缓存
        redisTemplate.boundHashOps(CacheKey.CAST_LIST).put(username, cartList);
    }


    @Override
    public void deleteCheckedCart(String username) {
        //获取未选中的购物车
        List<Map<String, Object>> cartList = findCartList(username).stream().filter(cart -> (boolean) cart.get("checked") == false).collect(Collectors.toList());
        redisTemplate.boundHashOps(CacheKey.CAST_LIST).put(username, cartList);//存入缓存
    }

    @Override
    public int preferential(String username) {
        //获取选中的购物车  List<OrderItem>  List<Map>
        List<TbOrderItem> orderItemList = findCartList(username).stream()
                .filter(cart -> (boolean) cart.get("checked") == true)
                .map(cart -> (TbOrderItem) cart.get("item"))
                .collect(Collectors.toList());
        //循环结果，统计每个分类的优惠金额，并累加
        int allPreMoney = 0;//累计优惠金额
        for (TbOrderItem orderItem : orderItemList) {
            allPreMoney += orderItem.getMoney() - orderItem.getPayMoney();
        }

        return allPreMoney;
    }

    @Override
    public List<Map<String, Object>> findNewOrderItemList(String username) {
        List<Map<String, Object>> cartList = findCartList(username);
        for (Map<String, Object> cart : cartList) {
            TbOrderItem orderItem = (TbOrderItem) cart.get("item");
            TbSku sku = skuService.findById(orderItem.getSkuId());
            orderItem.setPrice(sku.getPrice());
            orderItem.setMoney(sku.getPrice() * orderItem.getNum());
        }
        redisTemplate.boundHashOps(CacheKey.CAST_LIST).put(username, cartList);

        return cartList;
    }
}
