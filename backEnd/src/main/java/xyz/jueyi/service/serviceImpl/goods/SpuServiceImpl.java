package xyz.jueyi.service.serviceImpl.goods;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import xyz.jueyi.entity.goods.TbSpu;
import xyz.jueyi.mapper.goods.TbSpuMapper;
import xyz.jueyi.service.goods.SpuService;

import javax.annotation.Resource;

@Service
public class SpuServiceImpl extends ServiceImpl<TbSpuMapper, TbSpu> implements SpuService {

    @Resource
    private TbSpuMapper spuMapper;


    @Override
    public TbSpu findById(String id) {
        return spuMapper.selectById(id);
    }
}
