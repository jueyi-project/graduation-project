package xyz.jueyi.service;

import org.json.JSONObject;

public interface BaiduApiService {
    JSONObject getInfo(byte[] file, String idCardSide);
}
