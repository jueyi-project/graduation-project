package xyz.jueyi.service.order;

import com.baomidou.mybatisplus.extension.service.IService;
import xyz.jueyi.entity.PageResult;
import xyz.jueyi.entity.order.OrderAndItems;
import xyz.jueyi.entity.order.TbOrder;
import xyz.jueyi.entity.order.TbOrderItem;

import java.util.List;
import java.util.Map;

public interface OrderService extends IService<TbOrder> {
    Map<String, Object> add(TbOrder order);

    Map<String, Object> add(TbOrder order , TbOrderItem orderItem);

    int orderTimeOutLogic();

    void closeOrder(String username, String orderId);

    void delOrder(String username,String orderId);

    void updatePayOrder(String orderId, String transactionId);

    PageResult<TbOrder> findOrdersNotDel(String username, int page, int size);

    PageResult<TbOrder> findOrdersByOrderStatus(String username, int page, int size, String orderStatus);

    List<OrderAndItems> findOrderAndItems(String orderStatus, List<TbOrder> orderList);
}
