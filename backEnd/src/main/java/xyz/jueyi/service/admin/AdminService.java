package xyz.jueyi.service.admin;

import com.baomidou.mybatisplus.extension.service.IService;
import xyz.jueyi.entity.admin.TbAdmin;

public interface AdminService extends IService<TbAdmin> {
    TbAdmin findByPhone(String phone);

    TbAdmin findByUsername(String username);

}
