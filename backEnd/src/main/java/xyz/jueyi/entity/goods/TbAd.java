package xyz.jueyi.entity.goods;

import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;
import lombok.ToString;

@Data
@ToString
public class TbAd {

    @TableId
    private long id;
    private java.sql.Date startTime;
    private java.sql.Date endTime;
    private String status;
    private String image;


}
