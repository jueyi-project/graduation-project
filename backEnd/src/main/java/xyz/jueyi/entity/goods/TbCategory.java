package xyz.jueyi.entity.goods;

import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;
import lombok.ToString;

import java.io.Serializable;

@Data
@ToString
public class TbCategory implements Serializable {

  @TableId
  private int id;
  private String name;
  private int goodsNum;
  private String isShow;
  private String isMenu;
  private int parentId;


}
