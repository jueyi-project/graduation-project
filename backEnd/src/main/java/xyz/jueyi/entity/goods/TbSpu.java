package xyz.jueyi.entity.goods;

import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;
import lombok.ToString;

@Data
@ToString
public class TbSpu {

    @TableId
    private String id;
    private String name;
    private String caption;
    private String introduction;
    private String specItems;
    private int saleNum;
    private int commentNum;
    private String isMarketable;
    private String isDelete;
    private String status;
    private String isGroup;

}
