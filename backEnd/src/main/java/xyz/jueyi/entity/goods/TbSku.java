package xyz.jueyi.entity.goods;

import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;
import lombok.ToString;

@Data
@ToString
public class TbSku {

    @TableId
    private String id;
    private String name;
    private int price;
    private int num;
    private int alertNum;
    private String image;
    private String images;
    private int weight;
    private java.util.Date createTime;
    private java.util.Date updateTime;
    private String spuId;
    private int categoryId;
    private String categoryName;
    private String brandName;
    private String spec;
    private int saleNum;
    private String status;

}
