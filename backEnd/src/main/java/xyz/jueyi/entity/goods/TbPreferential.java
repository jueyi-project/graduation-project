package xyz.jueyi.entity.goods;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;
import lombok.ToString;

@Data
@ToString
public class TbPreferential {

    @TableId
    private int id;
    private int buyMoney;
    private int preMoney;
    private int categoryId;
    private java.sql.Date startTime;
    private java.sql.Date endTime;
    private String state;
    private String type;

    @TableField(exist = false)
    private String categoryName;


}
