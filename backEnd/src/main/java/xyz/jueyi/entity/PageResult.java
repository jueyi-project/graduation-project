package xyz.jueyi.entity;

import lombok.Data;
import lombok.ToString;

import java.io.Serializable;
import java.util.List;

/**
 * 分页结果
 *
 * @param <T>
 */
@Data
@ToString
public class PageResult<T> implements Serializable {

    private Long total;//返回记录数
    private List<T> rows;//结果

    public PageResult(Long total, List<T> rows) {
        this.total = total;
        this.rows = rows;
    }

    public PageResult() { }

}
