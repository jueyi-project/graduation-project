package xyz.jueyi.entity.order;

import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;
import lombok.ToString;

@Data
@ToString
public class TbOrderConfig {

  @TableId
  private int id;
  private int orderTimeout;
  private int takeTimeout;

}
