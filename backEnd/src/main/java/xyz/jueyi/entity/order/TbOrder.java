package xyz.jueyi.entity.order;

import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;
import lombok.ToString;

@Data
@ToString
public class TbOrder {

    @TableId
    private String id;
    private int totalNum;
    private int totalMoney;
    private int preMoney;
    private int postFee;
    private int payMoney;
    private String payType;
    private java.util.Date createTime;
    private java.util.Date payTime;
    private java.util.Date endTime;
    private java.util.Date updateTime;
    private java.util.Date closeTime;
    private String username;
    private String receiverContact;
    private String receiverMobile;
    private String receiverAddress;
    private String transactionId;
    private String orderStatus;
    private String payStatus;
    private int waitSend;
    private int waitReceive;
    private int waitReview;
    private String isDelete;
    private String isGroup;


}
