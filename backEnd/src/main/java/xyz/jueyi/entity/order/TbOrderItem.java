package xyz.jueyi.entity.order;

import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;
import lombok.ToString;

import java.io.Serializable;

@Data
@ToString
public class TbOrderItem implements Serializable {

    @TableId
    private String id;
    private String spuId;
    private String skuId;
    private String orderId;
    private int categoryId1;
    private int categoryId2;
    private int categoryId3;
    private String name;
    private int price;
    private int num;
    private int money;
    private int payMoney;
    private String image;
    private int weight;
    private int postFee;
    private String buyerMessage;
    private String invoice;
    private String consignStatus;
    private java.util.Date consignTime;
    private String shippingName;
    private String shippingCode;
    private String receiveStatus;
    private java.util.Date receiveTime;
    private String rateStatus;
    private java.util.Date rateTime;
    private String buyerRate;
    private String isReturn;


}
