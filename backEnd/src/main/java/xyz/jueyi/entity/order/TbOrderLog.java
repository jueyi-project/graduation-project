package xyz.jueyi.entity.order;


import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;
import lombok.ToString;

@Data
@ToString
public class TbOrderLog {

  @TableId
  private String id;
  private String operater;
  private java.util.Date operateTime;
  private String orderId;
  private String orderStatus;
  private String payStatus;
  private String consignStatus;
  private String remarks;

}
