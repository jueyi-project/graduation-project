package xyz.jueyi.entity.order;

import lombok.Data;
import lombok.ToString;

import java.io.Serializable;
import java.util.List;

/**
 * 组合实体类
 */
@Data
@ToString
public class OrderAndItems implements Serializable {
    private TbOrder order;
    private List<TbOrderItem> orderItemList;

}
