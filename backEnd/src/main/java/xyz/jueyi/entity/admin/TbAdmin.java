package xyz.jueyi.entity.admin;

import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;
import lombok.ToString;

import java.io.Serializable;

@Data
@ToString
public class TbAdmin implements Serializable {

    @TableId
    private long id;
    private String loginName;
    private String password;
    private String status;
    private String phone;

}
