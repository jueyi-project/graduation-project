package xyz.jueyi.entity.user;

import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;
import lombok.ToString;

@Data
@ToString
public class TbCities {

  @TableId
  private String cityid;
  private String city;
  private String provinceid;



}
