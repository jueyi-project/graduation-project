package xyz.jueyi.entity.user;


import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;
import lombok.ToString;

@Data
@ToString
public class TbAreas {

  @TableId
  private String areaid;
  private String area;
  private String cityid;

}
