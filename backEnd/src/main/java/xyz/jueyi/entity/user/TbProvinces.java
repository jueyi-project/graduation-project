package xyz.jueyi.entity.user;

import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;
import lombok.ToString;

@Data
@ToString
public class TbProvinces {

    @TableId
    private String provinceid;
    private String province;



}
