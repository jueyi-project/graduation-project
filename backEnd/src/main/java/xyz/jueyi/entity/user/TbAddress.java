package xyz.jueyi.entity.user;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;
import lombok.ToString;

@Data
@ToString
public class TbAddress {

  @TableId(type = IdType.AUTO)
  private long id;
  private String username;
  private String provinceid;
  private String cityid;
  private String areaid;
  private String phone;
  private String address;
  private String contact;
  private String isDefault;
  private String alias;



}
