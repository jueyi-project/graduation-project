package xyz.jueyi.entity.user;

import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;
import lombok.ToString;

@Data
@ToString
public class TbApplication {

    @TableId
    private int id;
    private String username;
    private java.util.Date applyTime;
    private String cardFront;
    private String cardBack;
    private String admin;
    private java.util.Date opreateTime;
    private String message;
    private String state;

}
