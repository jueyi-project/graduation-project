package xyz.jueyi.entity.user;

import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;
import lombok.ToString;

@Data
@ToString
public class TbUser {

    @TableId
    private String username;
    private String password;
    private String phone;
    private java.util.Date created;
    private java.util.Date updated;
    private String name;
    private String type;
    private String status;
    private String headPic;
    private java.util.Date lastLoginTime;

}
