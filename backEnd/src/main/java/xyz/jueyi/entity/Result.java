package xyz.jueyi.entity;

import lombok.Data;

import java.io.Serializable;

/**
 * 返回前端的消息封装
 */
@Data
public class Result implements Serializable {

    private int code;//返回的业务码  0：成功执行  1：发生错误
    private String message;//信息
    private String username;  //用户名
    private String type;   //用户类型

    public Result() {
        this.code = 0;
        this.message = "执行成功";
        this.username = "";
        this.type = "";
    }

    public Result(int code, String message) {
        this.code = code;
        this.message = message;
        this.username = "";
        this.type = "";
    }

    public Result(int code, String username, String type) {
        this.code = code;
        this.message = "执行成功";
        this.username = username;
        this.type = type;
    }
}
