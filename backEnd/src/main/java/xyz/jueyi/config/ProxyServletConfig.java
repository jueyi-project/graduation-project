package xyz.jueyi.config;

import com.google.common.collect.ImmutableMap;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.mitre.dsmiley.httpproxy.ProxyServlet;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

import javax.servlet.Servlet;
import java.util.Map;

@Component
@Data
@NoArgsConstructor
@AllArgsConstructor
@ConfigurationProperties(prefix = "proxy")
public class ProxyServletConfig {
    private String servlet_url1;
    private String servlet_url2;
    private String target_url1;
    private String target_url2;

    @Bean
    public Servlet createProxyServlet(){
        // 创建新的ProxyServlet
        return new ProxyServlet();
    }

    @Bean
    public ServletRegistrationBean proxyServletRegistration1(){
        ServletRegistrationBean registrationBean = new ServletRegistrationBean(createProxyServlet(), servlet_url1);
        registrationBean.setName("gdWeather");
        //设置网址以及参数
        Map<String, String> params = ImmutableMap.of(
                "targetUri", target_url1,
                "log", "true");
        registrationBean.setInitParameters(params);
        return registrationBean;
    }

    @Bean
    public ServletRegistrationBean proxyServletRegistration2(){
        ServletRegistrationBean registrationBean = new ServletRegistrationBean(createProxyServlet(), servlet_url2);
        registrationBean.setName("weibo");
        //设置网址以及参数
        Map<String, String> params = ImmutableMap.of(
                "targetUri", target_url2,
                "log", "true");
        registrationBean.setInitParameters(params);
        return registrationBean;
    }
}