package xyz.jueyi.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Data
@Component
@ConfigurationProperties(prefix = "idwork")
public class IdWorkerProperties {
    private long workerId;
    private long datacenterId;
}
