package xyz.jueyi.config;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;
import xyz.jueyi.entity.IdWorker;

/**
 * IdWorker实例对象的配置类
 */
@Component
public class IdWorkConfig {

    @Autowired
    private IdWorkerProperties idWorkerProperties;

    @Bean
    public IdWorker createWorker() {
       return new IdWorker(idWorkerProperties.getWorkerId(), idWorkerProperties.getDatacenterId());
       // return new IdWorker();
    }
}
