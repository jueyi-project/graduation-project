package xyz.jueyi.config;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * @Description: 阿里短信服务配置类
 * @Version: V1.0
 */
@Component
@Data
@NoArgsConstructor
@AllArgsConstructor
@ConfigurationProperties(prefix = "aliyun.oss.file")
public class AliyunOSSConfig {

    private String accessKeyId;

    private String accessKeySecret;

    private String endpointName;

    private String bucketName;

}
