package xyz.jueyi.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import xyz.jueyi.methods.SystemDateUtils;
import xyz.jueyi.service.order.OrderService;

@Component
@EnableScheduling
public class OrderTask {
    @Autowired
    private OrderService orderService;


    /**
     * 订单超时未付款，自动关闭，每分钟执行一次
     */
    @Scheduled(cron = "0 */1 * * * ?")
    public void orderTimeOutLogic() {
        System.out.println("定时任务1:" + SystemDateUtils.getStrDate());
        System.out.println("订单超时，关闭了" + orderService.orderTimeOutLogic() + "个订单");
    }

}
