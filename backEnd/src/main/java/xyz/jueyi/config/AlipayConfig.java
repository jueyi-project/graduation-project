package xyz.jueyi.config;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * @Description: 支付宝支付配置类
 * @author: jueyi
 */

@Component
@Data
@NoArgsConstructor
@AllArgsConstructor
@ConfigurationProperties(prefix = "alipay")
public class AlipayConfig {
    /**
     * 应用ID,您的APPID，收款账号既是您的APPID对应支付宝账号(此处用的是沙箱环境)
     */
    private String appid;
    /**
     * 商户私钥，您的PKCS8格式RSA2私钥，这些就是我们刚才设置的
     */
    private String private_key;
    /**
     * 支付宝公钥,查看地址：对应APPID下的支付宝公钥。，这些就是我们刚才设置的
     */
    private String ali_public_key;

    /**
     * 签名方式
     */
    private String sign_type;

    /**
     * 字符编码格式
     */
    private String charset;

    /**
     * 支付宝网关
     */
    private String gatewayUrl;


    /**
     * 页面跳转同步通知页面路径（自定义后端路径）,你的回调页面, 需http://格式的完整路径，不能加?id=123这类自定义参数，必须外网可以正常访问
     */
    private String return_url ;


}