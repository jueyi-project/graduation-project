package xyz.jueyi.controller.adminController;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;
import xyz.jueyi.entity.PageResult;
import xyz.jueyi.entity.Result;
import xyz.jueyi.entity.goods.TbAd;
import xyz.jueyi.service.goods.AdService;

@Secured("ROLE_ADMIN")
@RestController
@RequestMapping("/ad")
public class AdController {
    @Autowired
    private AdService adService;

    /**
     * 获得所有广告
     *
     * @return
     */
    @GetMapping("/getAds")
    public PageResult<TbAd> getAds(int page, int size) {
        return adService.getAds(page, size);
    }

    /**
     * 新增广告
     *
     * @param ad
     * @return
     */
    @PostMapping("/addAd")
    public Result addAd(@RequestBody TbAd ad) {
        adService.save(ad);
        return new Result();
    }

    /**
     * 修改广告
     *
     * @param ad
     * @return
     */
    @PostMapping("/updateAdById")
    public Result updateAdById(@RequestBody TbAd ad) {
        adService.updateById(ad);
        return new Result();
    }

    /**
     * 删除广告
     *
     * @param id
     * @return
     */
    @DeleteMapping("/deleteAdById")
    public Result deleteAdById(int id) {
        adService.removeById(id);
        return new Result();
    }

}
