package xyz.jueyi.controller.adminController;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;
import xyz.jueyi.entity.PageResult;
import xyz.jueyi.entity.Result;
import xyz.jueyi.entity.user.TbApplication;
import xyz.jueyi.entity.user.TbUser;
import xyz.jueyi.methods.GetUserNameUtils;
import xyz.jueyi.methods.SystemDateUtils;
import xyz.jueyi.service.user.ApplicationService;
import xyz.jueyi.service.user.UserService;

import java.util.List;

@RestController
@Secured("ROLE_ADMIN")
@RequestMapping("/application")
public class ApplicationController {
    @Autowired
    private ApplicationService applicationService;

    @Autowired
    private UserService userService;

    //获得未审核的卖家申请
    @GetMapping("/getNotReviewed")
    public PageResult<TbApplication> getNotReviewed(int page, int size) {
        return applicationService.getNotReviewed(page, size);
    }

    //获得已审核的卖家申请
    @GetMapping("/getReviewed")
    public PageResult<TbApplication> getReviewed(int page, int size) {
        return applicationService.getReviewed(page, size);
    }

    //进行审核
    @PostMapping("/review")
    public Result review(@RequestBody TbApplication application) {
        application.setAdmin(GetUserNameUtils.getUsernameFromSecurity());
        application.setOpreateTime(SystemDateUtils.getDate());
        applicationService.updateById(application);

        //如果同意
        if (application.getState().equals("2")) {
            TbUser user = userService.getById(application.getUsername());
            user.setType("1");
            userService.updateById(user);
        }

        return new Result();
    }
}
