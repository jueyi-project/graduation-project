package xyz.jueyi.controller.adminController;


import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import xyz.jueyi.entity.Result;
import xyz.jueyi.methods.GetUserNameUtils;

@RestController
@RequestMapping("/admin")
public class AdminController {

    @GetMapping("/getUsername")
    @Secured("ROLE_ADMIN")
    public Result getUsername() {
        String username = GetUserNameUtils.getUsernameFromSecurity();
        return new Result(0, username, null);
    }


}
