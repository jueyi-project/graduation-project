package xyz.jueyi.controller.adminController;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;
import xyz.jueyi.entity.PageResult;
import xyz.jueyi.entity.Result;
import xyz.jueyi.entity.goods.TbCategory;
import xyz.jueyi.entity.goods.TbPreferential;
import xyz.jueyi.service.goods.CategoryService;
import xyz.jueyi.service.goods.PreferentialService;

import java.util.List;

@RestController
@Secured("ROLE_ADMIN")
@RequestMapping("/preferential")
public class PreferentialController {

    @Autowired
    private PreferentialService preferentialService;

    @Autowired
    private CategoryService categoryService;

    /**
     * 无条件分页查询
     *
     * @param page
     * @param size
     * @return
     */
    @GetMapping("/getPreferential")
    public PageResult<TbPreferential> getPreferential(int page, int size) {
        return preferentialService.findPage(page, size);
    }

    /**
     * 根据分类id查询
     */
    @GetMapping("/getPreferentialByCategoryId")
    public TbPreferential getPreferentialByCategoryId(int categoryId) {
        return preferentialService.getPreferentialByCategoryId(categoryId);
    }

    /**
     * 用parentId获得分类列表
     *
     * @param parentId
     * @return
     */
    @GetMapping("/getCategoriesByParentId")
    public List<TbCategory> getCategoriesByParentId(int parentId) {
        return categoryService.getCategoriesByParentId(parentId);
    }

    /**
     * 更新优惠设置
     *
     * @param tbPreferential
     * @return
     */
    @PostMapping("/updatePreferential")
    public Result updatePreferential(@RequestBody TbPreferential tbPreferential) {
        preferentialService.updateById(tbPreferential);
        return new Result();
    }

    /**
     * 新增优惠设置
     *
     * @param tbPreferential
     * @return
     */
    @PostMapping("/addPreferential")
    public Result addPreferential(@RequestBody TbPreferential tbPreferential) {
        if (preferentialService.getPreferentialByCategoryId(tbPreferential.getCategoryId()) != null)
            throw new RuntimeException("该商品分类已设置过优惠活动！");

        tbPreferential.setBuyMoney(tbPreferential.getBuyMoney() * 100);
        tbPreferential.setPreMoney(tbPreferential.getPreMoney() * 100);
        preferentialService.save(tbPreferential);
        return new Result();
    }

    /**
     * 删除优惠设置
     *
     * @param id
     * @return
     */

    @GetMapping("/deletePreferentialById")
    public Result deletePreferentialById(int id) {
        preferentialService.removeById(id);
        return new Result();
    }

}
