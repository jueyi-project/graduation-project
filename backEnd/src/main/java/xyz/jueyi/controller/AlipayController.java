package xyz.jueyi.controller;

import com.alipay.api.response.AlipayUserInfoShareResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import xyz.jueyi.service.AliPayService;

@RestController
public class AlipayController {

    @Autowired
    private AliPayService aliPayService;


    @GetMapping("/userinfo")
    public AlipayUserInfoShareResponse userInfo(String auth_code) {
        String token = aliPayService.getToken(auth_code).getAccessToken();
        AlipayUserInfoShareResponse userInfo = aliPayService.getUserInfo(token);
        return userInfo;
    }


    @GetMapping("/payMoney")
    public String payMoney(String outTradeNo, String totalAmount) {
        Double money = Double.valueOf(totalAmount);
        totalAmount = (money / 100) + "";
        String s = aliPayService.alipay(outTradeNo, totalAmount, "支付订单", "支付订单");
        return s;
    }
}
