package xyz.jueyi.controller.userController;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCrypt;
import org.springframework.web.bind.annotation.*;
import xyz.jueyi.entity.Result;
import xyz.jueyi.entity.user.TbUser;
import xyz.jueyi.methods.SystemDateUtils;
import xyz.jueyi.service.user.UserService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.List;
import java.util.Map;


@RestController
@RequestMapping("/login")
public class LoginController {

    @Autowired
    private UserService userService;

    //普通用户登录验证
    @PostMapping("")
    public Result login(@RequestBody TbUser user, HttpServletRequest request) {
        TbUser checkUser = userService.findById(user.getUsername());
        if (checkUser == null) throw new RuntimeException("该用户未注册");
        if ("0".equals(checkUser.getStatus())) throw new RuntimeException("该账号因违反规定，已注销");

        boolean checkpw = BCrypt.checkpw(user.getPassword(), checkUser.getPassword());
        if (checkpw) {
            HttpSession session = request.getSession();
            session.setAttribute("username", user.getUsername());
            //设置登录时间
            checkUser.setLastLoginTime(SystemDateUtils.getDate());
            userService.updateById(checkUser);

            return new Result(0, checkUser.getUsername(), checkUser.getType());
        } else throw new RuntimeException("密码错误，请检查账号密码");
    }


    //商家用户密码登录
    @PostMapping("/sellerLoginByPsw")
    public Result sellerLoginByPsw(@RequestBody TbUser user, HttpServletRequest request) {
        TbUser checkUser = userService.findById(user.getUsername());
        if (checkUser == null) throw new RuntimeException("该用户未注册");
        if ("0".equals(checkUser.getStatus())) throw new RuntimeException("该账号因违反规定，已注销");
        if ("0".equals(checkUser.getType())) throw new RuntimeException("该账号未实名认证商家");

        boolean checkpw = BCrypt.checkpw(user.getPassword(), checkUser.getPassword());
        if (checkpw) {
            HttpSession session = request.getSession();
            session.setAttribute("username", user.getUsername());
            checkUser.setLastLoginTime(SystemDateUtils.getDate());
            userService.updateById(checkUser);

            return new Result(0, checkUser.getUsername(), checkUser.getType());
        } else throw new RuntimeException("密码错误，请检查账号密码");
    }

    //商家用户验证码登录
    @PostMapping("/sellerLoginByYZM")
    public Result sellerLoginByYZM(@RequestBody Map<String, String> map, HttpServletRequest request) {
        List<TbUser> userList = userService.findByPhone(map.get("phone"));

        if (userList.size() == 0) throw new RuntimeException("该手机未绑定过任何用户");

        TbUser checkUser = userList.get(0);
        if ("0".equals(checkUser.getStatus())) throw new RuntimeException("该账号因违反规定，已注销");
        if ("0".equals(checkUser.getType())) throw new RuntimeException("该账号未实名认证商家");

        HttpSession session = request.getSession();
        session.setAttribute("username", checkUser.getUsername());
        checkUser.setLastLoginTime(SystemDateUtils.getDate());
        userService.updateById(checkUser);

        return new Result(0, checkUser.getUsername(), checkUser.getType());

    }


    //退出登录
    @GetMapping("/logout")
    public Result logout(HttpServletRequest request) {
        request.getSession().removeAttribute("username");
        return new Result();
    }
}
