package xyz.jueyi.controller.userController;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import xyz.jueyi.entity.Result;
import xyz.jueyi.entity.user.TbAddress;
import xyz.jueyi.entity.user.TbAreas;
import xyz.jueyi.entity.user.TbCities;
import xyz.jueyi.entity.user.TbProvinces;
import xyz.jueyi.methods.GetUserNameUtils;
import xyz.jueyi.service.user.AddressService;
import xyz.jueyi.service.user.AreasService;
import xyz.jueyi.service.user.CitiesService;
import xyz.jueyi.service.user.ProvincesService;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/address")
public class UserAddressController {

    @Autowired
    private AddressService addressService;

    @Autowired
    private AreasService areasService;

    @Autowired
    private CitiesService citiesService;

    @Autowired
    private ProvincesService provincesService;

    //用户的所有收货地址
    @GetMapping("/findAddressByUsername")
    public List<TbAddress> findAddressByUsername(String username) {
        return addressService.findAddressByUsername(username);
    }

    //获得省、市、区
    @PostMapping("/findPCA")
    public String findPCA(@RequestBody Map<String, String> seachMap) {
        String province = provincesService.getById(seachMap.get("provinceid")).getProvince();
        String city = citiesService.getById(seachMap.get("cityid")).getCity();
        String area = areasService.getById(seachMap.get("areaid")).getArea();
        return province + city + area;
    }

    //所有的省
    @GetMapping("/findAllProvinces")
    public List<TbProvinces> findAllProvinces() {
        return provincesService.findAll();
    }

    //用省找市
    @GetMapping("/findCitiesByProvinceId")
    public List<TbCities> findCitiesByProvinceId(String provinceid) {
        return citiesService.findCitiesByProvinceId(provinceid);
    }


    //用市找区
    @GetMapping("/findAreasByCityId")
    public List<TbAreas> findAreasByCityId(String cityid) {
        return areasService.findAreasByCityId(cityid);
    }

    //删除收货地址
    @GetMapping("/deleteAddressById")
    public Result deleteAddressById(HttpServletRequest request, String id) {
        GetUserNameUtils.getUsernameFromSession(request);
        TbAddress address = addressService.getById(Integer.valueOf(id));
        if ("1".equals(address.getIsDefault())) throw new RuntimeException("不能删除默认地址");
        addressService.removeById(Integer.valueOf(id));
        return new Result();
    }

    //修改默认收货地址
    @GetMapping("/changeToDefault")
    public Result changeToDefault(HttpServletRequest request, String id, String username) {
        GetUserNameUtils.getUsernameFromSession(request);
        //取消默认
        if (addressService.findByUsernameAndDefault(username) != null) {
            TbAddress aDefault = addressService.findByUsernameAndDefault(username);
            aDefault.setIsDefault("0");
            addressService.updateById(aDefault);
        }
        //设置默认
        TbAddress address = addressService.getById(Integer.valueOf(id));
        address.setIsDefault("1");
        addressService.updateById(address);
        return new Result();
    }


    //添加收货地址
    @PostMapping("/addAddress")
    public Result addAddress(HttpServletRequest request, @RequestBody TbAddress address) {
        String username = GetUserNameUtils.getUsernameFromSession(request);
        //若该用户没有地址，就把这条设置为默认
        List<TbAddress> addressList = addressService.findAddressByUsername(username);
        if (addressList.size() == 0) {
            address.setIsDefault("1");
            addressService.save(address);
            return new Result(0, "此地址已设置为默认地址");
        } else {
            //若要设置为默认地址
            if ("1".equals(address.getIsDefault())) {
                address.setIsDefault("0");
                addressService.save(address);
                changeToDefault(request, address.getId() + "", address.getUsername());
                return new Result(0, "添加成功，并设置为默认地址");
            } else {
                addressService.save(address);
                return new Result(0, "添加成功");
            }
        }
    }

    //修改收货地址
    @PostMapping("/updateAddress")
    public Result updateAddress(HttpServletRequest request, @RequestBody TbAddress address) {
        GetUserNameUtils.getUsernameFromSession(request);
        //若要设置为默认地址
        if ("1".equals(address.getIsDefault())) {
            address.setIsDefault("0");
            addressService.updateById(address);
            changeToDefault(request, address.getId() + "", address.getUsername());
            return new Result(0, "修改成功，并设置为默认地址");
        } else {
            addressService.updateById(address);
            return new Result(0, "修改成功");
        }
    }


}