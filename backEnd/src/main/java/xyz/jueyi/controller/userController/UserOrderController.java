package xyz.jueyi.controller.userController;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import xyz.jueyi.entity.PageResult;
import xyz.jueyi.entity.order.OrderAndItems;
import xyz.jueyi.entity.order.TbOrder;
import xyz.jueyi.service.order.OrderService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.List;

@RestController
@RequestMapping("/userOrder")
public class UserOrderController {

    @Autowired
    private OrderService orderService;

    /**
     * 根据username查出未删除的order
     *
     * @param request
     * @param page
     * @param size
     * @return
     */
    @GetMapping("/findOrders")
    public PageResult<TbOrder> findOrders(HttpServletRequest request, int page, int size) {
        HttpSession session = request.getSession();
        String username = (String) session.getAttribute("username");
        return orderService.findOrdersNotDel(username, page, size);
    }

    /**
     * 根据订单状态查找订单，0-待付款，1-待发货，2-待收货，3-待评价
     *
     * @param request
     * @param page
     * @param size
     * @return
     */
    @GetMapping("/findOrdersByStatus")
    public PageResult<TbOrder> findOrdersByStatus(HttpServletRequest request, int page, int size, String orderStatus) {
        HttpSession session = request.getSession();
        String username = (String) session.getAttribute("username");
        return orderService.findOrdersByOrderStatus(username, page, size, orderStatus);
    }

    /**
     * 根据前端返回的orderList返回orderAndItemsList
     *
     * @param orderList
     * @return
     */
    @PostMapping("/findOrderAndItems")
    public List<OrderAndItems> findOrderAndItems(String orderStatus, @RequestBody List<TbOrder> orderList) {
        return orderService.findOrderAndItems(orderStatus,orderList);
    }

}


