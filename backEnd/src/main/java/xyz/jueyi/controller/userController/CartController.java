package xyz.jueyi.controller.userController;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import xyz.jueyi.entity.Result;
import xyz.jueyi.methods.GetUserNameUtils;
import xyz.jueyi.service.user.CartService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/cart")
public class CartController {

    @Autowired
    private CartService cartService;

    /**
     * 购物车所有商品
     *
     * @return
     */
    @GetMapping("/findCartList")
    public List<Map<String, Object>> findCartList(HttpServletRequest request) {
        HttpSession session = request.getSession();
        String username = (String) session.getAttribute("username");
        if (username == null) return new ArrayList<>();
        List<Map<String, Object>> cartList = cartService.findCartList(username);
        return cartList;
    }

    /**
     * 购物车添加商品
     */
    @GetMapping("/buy")
    public Result buy(HttpServletRequest request, String skuId, Integer num) {
        String username = GetUserNameUtils.getUsernameFromSession(request);
        cartService.addGoods(username, skuId, num);
        return new Result();
    }

    /**
     * 购物车商品数量修改
     */
    @GetMapping("/updateNum")
    public Result updateNum(HttpServletRequest request, String skuId, Integer num) {
        String username = GetUserNameUtils.getUsernameFromSession(request);
        cartService.updateNum(username, skuId, num);
        return new Result();
    }

    /**
     * 是否开发票状态修改
     */
    @GetMapping("/updateInvoice")
    public Result updateInvoice(HttpServletRequest request, String skuId) {
        String username = GetUserNameUtils.getUsernameFromSession(request);
        cartService.updateInvoice(username, skuId);
        return new Result();
    }

    /**
     * 买家留言修改
     */
    @GetMapping("/updateBuyerMsg")
    public Result updateBuyerMsg(HttpServletRequest request, String skuId, String msg) {
        String username = GetUserNameUtils.getUsernameFromSession(request);
        cartService.updateBuyerMessage(username, skuId, msg);
        return new Result();
    }


    /**
     * 更新购物车中被选中的商品
     *
     * @param skuList
     * @return
     */
    @PostMapping("/updateCheckedByList")
    public Result updateCheckedByList(HttpServletRequest request, @RequestBody List<String> skuList) {
        String username = GetUserNameUtils.getUsernameFromSession(request);
        cartService.updateCheckedByList(username, skuList);
        return new Result();
    }


    /**
     * 删除购物车中被选中的商品
     */
    @GetMapping("/deleteCheckedCart")
    public Result deleteCheckedCart(HttpServletRequest request) {
        String username = GetUserNameUtils.getUsernameFromSession(request);
        cartService.deleteCheckedCart(username);
        return new Result();
    }

    /**
     * 计算购物车优惠金额
     */
    @GetMapping("/preferential")
    public Map preferential(HttpServletRequest request) {
        String username = GetUserNameUtils.getUsernameFromSession(request);
        int preferential = cartService.preferential(username);
        Map map = new HashMap();
        map.put("preferential", preferential);
        return map;
    }

    /**
     * 获取选中的购物车列表
     */
    @GetMapping("/findNewOrderItemList")
    public List<Map<String, Object>> findNewOrderItemList(HttpServletRequest request) {
        HttpSession session = request.getSession();
        String username = (String) session.getAttribute("username");
        if (username == null) return new ArrayList<>();
        return cartService.findNewOrderItemList(username);
    }
}
