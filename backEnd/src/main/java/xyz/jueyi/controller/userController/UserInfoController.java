package xyz.jueyi.controller.userController;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.crypto.bcrypt.BCrypt;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import xyz.jueyi.entity.Result;
import xyz.jueyi.entity.user.TbUser;
import xyz.jueyi.methods.GetClientIpUtils;
import xyz.jueyi.methods.GetUserNameUtils;
import xyz.jueyi.service.user.UserService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/userinfo")
public class UserInfoController {

    @Autowired
    private UserService userService;

    //获取当前登录用户信息
    @GetMapping("/getUser")
    public TbUser getUser(HttpServletRequest request) {
        HttpSession session = request.getSession();
        String username = (String) session.getAttribute("username");
        if (username == null) return new TbUser();
        TbUser user = userService.findById(username);
        user.setPassword(null);
        return user;
    }

    //获取所有用户
    @Secured("ROLE_ADMIN")
    @GetMapping("/getAllUser")
    public List<TbUser> getAllUser() {
        return userService.findAll();
    }

    //忘记密码——获取手机
    @GetMapping("/getUserPhone")
    public Map getUserPhone(String username) {
        TbUser checkUser = userService.findById(username);
        if (checkUser == null) throw new RuntimeException("该用户未注册");
        if ("0".equals(checkUser.getStatus())) throw new RuntimeException("该账号因违反规定，已注销");
        Map map = new HashMap();
        map.put("code", 0);
        map.put("phone", checkUser.getPhone());
        return map;
    }

    //获取用户登录的ip
    @GetMapping("/getUserIP")
    public Map getUserIp(HttpServletRequest request) {
        Map map = new HashMap();
        map.put("code", 0);
        map.put("ip", GetClientIpUtils.getRemortIP(request));
        return map;
    }

    //修改真实姓名
    @GetMapping("/updateName")
    public Result updateName(HttpServletRequest request, String name) {
        HttpSession session = request.getSession();
        String username = (String) session.getAttribute("username");
        userService.updateName(name, username);
        return new Result();
    }

    //修改头像
    @GetMapping("/updateHeadPic")
    public Result updateHeadPic(HttpServletRequest request, String headPic) {
        String username = GetUserNameUtils.getUsernameFromSession(request);
        userService.updateHeadPic(headPic, username);
        return new Result();
    }

    //修改手机号码
    @GetMapping("/updatePhone")
    public Result updatePhone(HttpServletRequest request, String phone) {
        String username = GetUserNameUtils.getUsernameFromSession(request);
        List<TbUser> userList = userService.findByPhone(phone);
        if (userList.size() != 0) throw new RuntimeException("该手机已绑定过其他账号");
        else userService.updatePhone(phone, username);
        return new Result();
    }

    //修改密码
    @GetMapping("/updatePassword")
    public Result updatePassword(HttpServletRequest request, String oldPsw, String newPsw) {
        String username = GetUserNameUtils.getUsernameFromSession(request);
        TbUser checkUser = userService.findById(username);
        boolean checkpw = BCrypt.checkpw(oldPsw, checkUser.getPassword());

        if (!checkpw) {
            throw new RuntimeException("密码错误，请检查账号密码");
        } else {
            String gensalt = BCrypt.gensalt();
            String password = BCrypt.hashpw(newPsw, gensalt);
            userService.updatePsw(password, username);
        }
        return new Result();
    }

    //忘记密码——修改密码
    @GetMapping("/updatePsw")
    public Result updatePsw(String username, String newPsw) {
        String gensalt = BCrypt.gensalt();
        String password = BCrypt.hashpw(newPsw, gensalt);
        userService.updatePsw(password, username);

        return new Result();
    }

    //启用禁用账号
    @Secured("ROLE_ADMIN")
    @GetMapping("/updateUserStatus")
    public Result updateUserStatus(String username, String status) {
        TbUser user = userService.getById(username);
        user.setStatus(status);
        userService.updateById(user);
        return new Result();
    }

}
