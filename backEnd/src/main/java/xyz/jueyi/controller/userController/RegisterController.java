package xyz.jueyi.controller.userController;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCrypt;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import xyz.jueyi.entity.Result;
import xyz.jueyi.entity.user.TbApplication;
import xyz.jueyi.entity.user.TbUser;
import xyz.jueyi.methods.GetUserNameUtils;
import xyz.jueyi.methods.SystemDateUtils;
import xyz.jueyi.service.user.ApplicationService;
import xyz.jueyi.service.user.UserService;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

@RestController
@RequestMapping("/register")
public class RegisterController {
    @Autowired
    private UserService userService;

    @Autowired
    private ApplicationService applicationService;

    //普通用户注册
    @PostMapping("/addUser")
    public Result register(@RequestBody Map<String, String> form) {
        if (userService.findById(form.get("username")) != null) throw new RuntimeException("该用户已存在");
        else if (userService.findByPhone(form.get("phone")).size() != 0) throw new RuntimeException("该手机已注册用户");
        else {
            String gensalt = BCrypt.gensalt();
            String password = BCrypt.hashpw(form.get("password"), gensalt);

            TbUser user = new TbUser();
            user.setUsername(form.get("username"));
            user.setPassword(password);
            user.setPhone(form.get("phone"));
            user.setCreated(SystemDateUtils.getDate());
            user.setUpdated(SystemDateUtils.getDate());
            user.setType("0");
            user.setStatus("1");
            userService.add(user);
        }
        return new Result(0, "注册成功！");
    }

    //申请认证商家
    @PostMapping("/toBeSeller")
    public Result toBeSeller(HttpServletRequest request, @RequestBody TbApplication application) {
        String username = GetUserNameUtils.getUsernameFromSession(request);
        if (applicationService.findApplicationByState(username, "0") != null)
            throw new RuntimeException("请勿重复申请，请耐心等待！");
        if (applicationService.findApplicationByState(username, "2") != null)
            throw new RuntimeException("您已经是商家了，无需再次申请！");

        application.setUsername(username);
        application.setApplyTime(SystemDateUtils.getDate());
        application.setState("0");  //已申请
        applicationService.save(application);
        return new Result();
    }


}
