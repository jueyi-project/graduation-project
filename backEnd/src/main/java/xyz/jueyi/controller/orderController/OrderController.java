package xyz.jueyi.controller.orderController;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import xyz.jueyi.entity.Result;
import xyz.jueyi.entity.order.TbOrder;
import xyz.jueyi.entity.order.TbOrderItem;
import xyz.jueyi.methods.GetUserNameUtils;
import xyz.jueyi.service.order.OrderService;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

@RestController
@RequestMapping("/order")
public class OrderController {

    @Autowired
    private OrderService orderService;

    /**
     * 从购物车下单
     *
     * @param request
     * @param order
     * @return
     */
    @PostMapping("/saveOrder")
    public Map<String, Object> saveOrder(HttpServletRequest request, @RequestBody TbOrder order) {
        String username = GetUserNameUtils.getUsernameFromSession(request);
        order.setUsername(username);
        return orderService.add(order);
    }

    /**
     * 从立即购买下单
     *
     * @param request
     * @param data
     * @return
     */
    @PostMapping("/saveOrderNow")
    public Map<String, Object> saveOrderNow(HttpServletRequest request, @RequestBody String data) {
        String username = GetUserNameUtils.getUsernameFromSession(request);
        JSONObject object = JSON.parseObject(data); //解析成json对象
        TbOrder order = JSON.parseObject(object.getString("order"), TbOrder.class);
        order.setUsername(username);
        TbOrderItem orderItem = JSON.parseObject(object.getString("orderItem"), TbOrderItem.class);
        return orderService.add(order, orderItem);
    }


    /**
     * 取消订单
     *
     * @param request
     * @param orderId
     * @return
     */
    @GetMapping("/cancelOrder")
    public Result cancelOrder(HttpServletRequest request, String orderId) {
        String username = GetUserNameUtils.getUsernameFromSession(request);
        orderService.closeOrder(username, orderId);
        return new Result();
    }

    /**
     * 删除订单
     *
     * @param request
     * @param orderId
     * @return
     */
    @GetMapping("/delOrder")
    public Result delOrder(HttpServletRequest request, String orderId) {
        String username = GetUserNameUtils.getUsernameFromSession(request);
        orderService.delOrder(username, orderId);
        return new Result();
    }


}