package xyz.jueyi.controller.orderController;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import xyz.jueyi.entity.Result;
import xyz.jueyi.methods.GetUserNameUtils;
import xyz.jueyi.service.order.OrderService;

import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping("/pay")
public class PayOrderController {

    @Autowired
    private OrderService orderService;

    @GetMapping("/updatePayOrder")
    public Result updatePayOrder(String orderId, String transactionId, HttpServletRequest request) {
        GetUserNameUtils.getUsernameFromSession(request);
        orderService.updatePayOrder(orderId, transactionId);
        return new Result();
    }

}