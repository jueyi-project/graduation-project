package xyz.jueyi.controller;

import com.alibaba.fastjson.JSON;
import com.aliyuncs.dysmsapi.model.v20170525.QuerySendDetailsResponse;
import com.aliyuncs.dysmsapi.model.v20170525.SendSmsResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import xyz.jueyi.entity.user.TbUser;
import xyz.jueyi.service.AliyunSmsSenderService;
import xyz.jueyi.service.user.UserService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.HashMap;
import java.util.Map;

/**
 * @Description: 测试发送短信controller
 * @Version: V1.0
 */
@RestController
@RequestMapping("/sms")
public class SmsController {

    @Autowired
    private AliyunSmsSenderService aliyunSmsSenderService;

    @Autowired
    private UserService userService;


    /**
     * @Description: 短信发送
     * @Version: V1.0
     */
    @GetMapping("/userRegist")
    @ResponseBody
    public String sendVerifyCode(String phone) {
        Map<String, String> map = new HashMap<>();
        String code = (int) ((Math.random() * 9 + 1) * 100000) + "";
        map.put("code", code);
        SendSmsResponse sendSmsResponse = aliyunSmsSenderService.sendSms(phone,
                JSON.toJSONString(map), "SMS_234136503");
        sendSmsResponse.setCode(code);
        return JSON.toJSONString(sendSmsResponse);
    }

    /**
     * @Description: 短信发送
     * @Version: V1.0
     */
    @GetMapping("/userUpdatePhone")
    @ResponseBody
    public String userUpdatePhone(HttpServletRequest request) {
        HttpSession session = request.getSession();
        String username = (String) session.getAttribute("username");
        TbUser user = userService.findById(username);

        Map<String, String> map = new HashMap<>();
        String code = (int) ((Math.random() * 9 + 1) * 100000) + "";
        map.put("code", code);
        SendSmsResponse sendSmsResponse = aliyunSmsSenderService.sendSms(user.getPhone(),
                JSON.toJSONString(map), "SMS_234136503");
        sendSmsResponse.setCode(code);
        return JSON.toJSONString(sendSmsResponse);
    }


    /**
     * @Description: 短信查询
     * @Version: V1.0
     */
    @GetMapping("/query")
    @ResponseBody
    public String query() {
        QuerySendDetailsResponse querySendDetailsResponse = aliyunSmsSenderService.querySendDetails("734820044394829190^0",
                "15842465267", 10L, 1L);
        return JSON.toJSONString(querySendDetailsResponse);
    }
}

