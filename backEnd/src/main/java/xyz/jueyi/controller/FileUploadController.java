package xyz.jueyi.controller;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import xyz.jueyi.entity.Result;
import xyz.jueyi.methods.GetUserNameUtils;
import xyz.jueyi.service.AliyunOssService;
import xyz.jueyi.service.BaiduApiService;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("/oss")
public class FileUploadController {

    @Autowired
    private AliyunOssService ossService;

    @Autowired
    private BaiduApiService baiduApiService;

    //上传头像
    @PostMapping("/uploadAvatar")
    public Result uploadAvatar(HttpServletRequest request, @RequestParam("file") MultipartFile file) {
        String username = GetUserNameUtils.getUsernameFromSession(request);
        String url = ossService.uploadAvatar(username, file);
        return new Result(0, url);
    }

    //上传身份证正面
    @PostMapping("/uploadCardFront")
    public Map uploadCardFront(String username, @RequestParam("file") MultipartFile file) {
        if (StringUtils.isEmpty(username)) throw new RuntimeException("未登录");
        Map map = new HashMap();
        JSONObject result;
        try {
            result = baiduApiService.getInfo(file.getBytes(), "front");
        } catch (IOException e) {
            map.put("code", 1);
            map.put("message", e);
            return map;
        }
        if (result == null) throw new RuntimeException("上传的图片不符合要求！");

        map.put("result", result.toString(2));
        String url = ossService.uploadCardFront(username, file);
        map.put("url", url);
        map.put("code", 0);
        return map;
    }

    //上传身份证背面
    @PostMapping("/uploadCardBack")
    public Map uploadCardBack(String username, @RequestParam("file") MultipartFile file) {
        if (StringUtils.isEmpty(username)) throw new RuntimeException("未登录");
        Map map = new HashMap();
        JSONObject result;
        try {
            result = baiduApiService.getInfo(file.getBytes(), "back");
        } catch (IOException e) {
            map.put("code", 1);
            map.put("message", e);
            return map;
        }
        if (result == null) throw new RuntimeException("上传的图片不符合要求！");

        map.put("result", result.toString(2));
        String url = ossService.uploadCardBack(username, file);
        map.put("url", url);
        map.put("code", 0);
        return map;
    }

    //上传广告图片
    @PostMapping("/uploadAdPhoto")
    public Result uploadAdPhoto(@RequestParam("file") MultipartFile file) {
        String url = ossService.uploadAdPhoto(file);
        return new Result(0, url);
    }

    @GetMapping("/uploadTest")
    public Result uploadTest() {
        String url = ossService.uploadTest();
        return new Result(0, url);
    }
}

