package xyz.jueyi.controller.goodsController;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import xyz.jueyi.entity.PageResult;
import xyz.jueyi.entity.goods.TbPreferential;
import xyz.jueyi.entity.goods.TbSku;
import xyz.jueyi.entity.goods.TbSpu;
import xyz.jueyi.service.goods.AdService;
import xyz.jueyi.service.goods.PreferentialService;
import xyz.jueyi.service.goods.SkuService;
import xyz.jueyi.service.goods.SpuService;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/goods")
public class GoodsController {

    @Autowired
    private SkuService skuService;

    @Autowired
    private SpuService spuService;

    @Autowired
    private PreferentialService preferentialService;

    @Autowired
    private AdService adService;


    /**
     * 首页商品数据
     *
     * @param page
     * @param size
     * @return
     */
    @GetMapping("/fetchData")
    public PageResult<TbSku> findPage(int page, int size) {
        return skuService.findPage(page, size);
    }

    /**
     * 商品详情
     *
     * @param sku_id
     * @return
     */
    @GetMapping("/goodsDetails")
    public Map goodsDetails(String sku_id) {
        Map res = new HashMap();
        TbSku sku = skuService.findById(sku_id);
        TbSpu spu = spuService.findById(sku.getSpuId());
        res.put("sku", sku);
        res.put("spu", spu);
        return res;
    }

    /**
     * 商品优惠
     *
     * @param categoryId
     * @param money
     * @return
     */
    @GetMapping("/getPreferential")
    public Map getPreferential(Integer categoryId, int money) {
        int preMoney = preferentialService.findPreMoneyByCategoryId(categoryId, money);
        Map map = new HashMap();
        map.put("code", 0);
        map.put("preMoney", preMoney);
        return map;
    }

    /**
     * 用skuId查询商品是否参加优惠活动
     *
     * @param sku_id
     * @return
     */
    @GetMapping("/hasPreferential")
    public TbPreferential hasPreferential(String sku_id) {
        TbSku sku = skuService.findById(sku_id);
        if (preferentialService.findByCategoryId(sku.getCategoryId()) == null) return null;
        else return preferentialService.findByCategoryId(sku.getCategoryId());
    }

    /**
     * 搜索结果数据
     */
    @PostMapping("/searchGoods")
    public PageResult<TbSku> searchGoods(@RequestBody Map<String, String> searchMap) {
        String str = searchMap.get("str");
        int page = Integer.parseInt(searchMap.get("page"));
        int size = Integer.parseInt(searchMap.get("size"));
        return skuService.searchPage(str, page, size);
    }

    /**
     * 轮播图
     */
    @GetMapping("/carousel")
    public List carousel() {
        return adService.getCarousels();
    }


}
