package xyz.jueyi.methods;

import javax.servlet.http.HttpServletRequest;

public class GetClientIpUtils {
    //获取用户真实ip地址
    public static String getRemortIP(HttpServletRequest request) {
        if (request.getHeader("x-forwarded-for") == null) {
            return request.getRemoteAddr();
        }
        return request.getHeader("x-forwarded-for");
    }
}
