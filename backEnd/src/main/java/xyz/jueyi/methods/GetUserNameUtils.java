package xyz.jueyi.methods;

import org.springframework.security.core.context.SecurityContextHolder;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

public class GetUserNameUtils {
    public static String getUsernameFromSession(HttpServletRequest request) {
        HttpSession session = request.getSession();
        String username = (String) session.getAttribute("username");
        if (username == null) throw new RuntimeException("未登录");
        else return username;
    }

    public static String getUsernameFromSecurity() {
        return SecurityContextHolder.getContext().getAuthentication().getName();
    }
}
