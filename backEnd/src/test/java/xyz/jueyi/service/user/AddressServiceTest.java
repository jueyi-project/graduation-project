package xyz.jueyi.service.user;

import lombok.extern.log4j.Log4j2;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import xyz.ServerApplication;
import xyz.jueyi.entity.user.TbAddress;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = {ServerApplication.class})
@Log4j2
class AddressServiceTest {

    @Autowired
    private AddressService addressService;

    @Test
    void findAddressByUsername() {
       List<TbAddress> addresses = addressService.findAddressByUsername("jueyi");
        System.out.println(addresses);
    }
}