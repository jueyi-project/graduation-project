package xyz.jueyi.service.order;

import lombok.extern.log4j.Log4j2;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import xyz.ServerApplication;
import xyz.jueyi.entity.PageResult;
import xyz.jueyi.entity.order.TbOrder;
import xyz.jueyi.mapper.goods.TbCategoryMapper;
import xyz.jueyi.mapper.goods.TbSkuMapper;

import javax.annotation.Resource;
import java.util.List;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = {ServerApplication.class})
@Log4j2
class OrderServiceTest {
    @Autowired
    private OrderService orderService;

    @Resource
    private TbSkuMapper skuMapper;

    @Resource
    private TbCategoryMapper categoryMapper;

    @Test
    void add() {
        TbOrder order = new TbOrder();
        order.setUsername("jueyi");
        orderService.add(order);
    }

    @Test
    void updatePayOrder() {
        orderService.updatePayOrder("1500061946890817536", "111111");
    }




    @Test
    void findOrdersNotDel() {
        PageResult result = orderService.findOrdersNotDel("jueyi", 1, 5);
        System.out.println(result);
    }


}