package xyz.jueyi;

import xyz.ServerApplication;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import lombok.extern.log4j.Log4j2;

/**
 * @Description: log4j2整合测试
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = {ServerApplication.class})
@Log4j2
public class Log4jTest {
    @Test
    public void logTest(){
        log.error("测试日志");
    }
}
