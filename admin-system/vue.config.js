module.exports = {
    publicPath: "/",
    outputDir: 'dist',
    devServer: {
        port: 8081,
        host: '0.0.0.0',
        https: false,
        open: true,
        proxy: {
             '/api': {
                 target: 'https://localhost:8443',
                 // target: 'https://jueyi.xyz:8443',
                 changeOrigin: true,
                 ws: true,
                 pathRewrite: {
                     '^/api': '',
                 }
             },
            '/gdWeather': {
                target: 'https://restapi.amap.com/v3',
                changeOrigin: true,
                pathRewrite: {
                    '^/gdWeather': ''
                }
            },
            '/weibo': {
                target: 'https://tenapi.cn',
                changeOrigin: true,
                pathRewrite: {
                    '^/weibo': ''
                }
            }
        }
    }
}
