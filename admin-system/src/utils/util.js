import Moment from "moment";
import {getUserIP} from "@/utils/api/userinfo";
import {getAdcode, getGodeWeather} from "@/utils/api/others";

export function timeFormat(time) {
    return Moment(time).format("YYYY-MM-DD HH:mm:ss");
}


export async function getWeather() {
    let key = "fee9a1d38995b2619dec08df52135bf3";
    //先获取ip,本地测试的时候，ip为127.0.0.1 无法获取天气
    let res = await getUserIP();
    if (res.code === 0) {
        //获取adcode
        res = await getAdcode({key: key, ip: res.ip});
        if (res.adcode.length === 0) res.adcode = '110101'
        //返回天气信息
        return getGodeWeather({key: key, city: res.adcode});
    }

}


