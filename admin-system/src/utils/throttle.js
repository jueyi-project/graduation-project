export function throttle(func, delay =3000) {
    let pre = 0;
    return function () {
        let now = new Date();
        let context =this;
        let args=arguments;
        if (now - pre > delay) {
            func.apply(context,args);
            pre = now;
        }

    }
}
