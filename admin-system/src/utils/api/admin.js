import request from './request'

//登录，这个有点特殊，用的不是data
export const login = (params) => request({url: '/admin/login', method: 'post', params});

//获取登录用户名
export const getUsername = () => request({url: '/admin/getUsername', method: 'get'});

