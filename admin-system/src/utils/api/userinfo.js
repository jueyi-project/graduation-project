import request from "@/utils/api/request";

//获取用户ip
export const getUserIP = () => request({url: '/userinfo/getUserIP', method: "get"});

//获取全部用户信息
export const getAllUser = () => request({url: '/userinfo/getAllUser', method: 'get'});

//启用禁用账号
export const updateUserStatus = (params) => request({url: '/userinfo/updateUserStatus', method: 'get', params});