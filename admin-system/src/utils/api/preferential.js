import request from "@/utils/api/request";

//获取满减优惠信息
export const getPreferential = (params) => request({
    url: "/preferential/getPreferential",
    method: "get",
    params
});

//根据分类id查询
export const getPreferentialByCategoryId = (params) => request({
    url: "/preferential/getPreferentialByCategoryId",
    method: "get",
    params
});

//用parentId获得分类列表
export const getCategoriesByParentId = (params) => request({
    url: "/preferential/getCategoriesByParentId",
    method: "get",
    params
});


//更新优惠设置
export const updatePreferential = (data) => request({
    url: "/preferential/updatePreferential",
    method: "post",
    data
})

//新增优惠设置
export const addPreferential = (data) => request({
    url: "/preferential/addPreferential",
    method: "post",
    data
})

//删除优惠设置
export const deletePreferentialById = (params) => request({
    url: "/preferential/deletePreferentialById",
    method: "get",
    params
})
