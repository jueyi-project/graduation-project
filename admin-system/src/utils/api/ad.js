import request from "@/utils/api/request";

//获得所有广告
export const getAds = (params) => request({
    url: "/ad/getAds",
    method: "get",
    params
});

//新增广告
export const addAd = (data) => request({
    url: "/ad/addAd",
    method: "post",
    data
})

//修改广告
export const updateAdById = (data) => request({
    url: "/ad/updateAdById",
    method: "post",
    data
})

//删除广告
export const deleteAdById = (params) => request({
    url: "/ad/deleteAdById",
    method: "delete",
    params
})