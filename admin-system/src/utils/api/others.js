import {request_gaodeMap, request_weibo} from './request'

//获取定位
export const getAdcode = (params) => request_gaodeMap({url: "/ip", method: "get", params});

//获取天气
export const getGodeWeather = (params) => request_gaodeMap({url: "/weather/weatherInfo", method: "get", params});

//获取热搜
export const getHot = () => request_weibo({url: "/resou/", method: "get"});