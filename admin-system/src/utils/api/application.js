import request from "@/utils/api/request";

//获得未审核的卖家申请
export const getNotReviewed = (params) => request({
    url: "/application/getNotReviewed",
    method: "get",
    params
});

//获得已审核的卖家申请
export const getReviewed = (params) => request({
    url: "/application/getReviewed",
    method: "get",
    params
});

//进行审核
export const review = (data) => request({
    url: "/application/review",
    method: "post",
    data
});