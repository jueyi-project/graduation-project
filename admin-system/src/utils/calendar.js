import moment from "moment";

function createElement(tagName, className, innerText) {
    let ele = document.createElement(tagName);
    if (className) {
        ele.className = className;
    }
    if (innerText) {
        ele.innderText = ele.textContent = innerText;
    }
    return ele;
}

export class Calendar {
    constructor(selector, events) {
        this.el = document.querySelector(selector);
        this.events = events;
        this.today = moment();
        this.current = moment().date(1);
        this.draw();
        let current = document.querySelector('.today');
        if (current) {
            let self = this;
            setTimeout(function () {
                self.openDay(current);
            }, 500);
        }
    }

    draw() {
        this.drawHeader();
        this.drawMonth();
        this.drawLegend();
    }

    drawHeader() {
        let self = this;
        if (!this.header) {
            //Create the header elements
            this.header = createElement('div', 'calendar-header');
            this.header.className = 'calendar-header';

            this.title = createElement('h1');

            let right = createElement('div', 'calendar-right');
            right.addEventListener('click', function () {
                self.nextMonth();
            });

            let left = createElement('div', 'calendar-left');
            left.addEventListener('click', function () {
                self.prevMonth();
            });

            //Append the Elements
            this.header.appendChild(this.title);
            this.header.appendChild(right);
            this.header.appendChild(left);
            this.el.appendChild(this.header);
        }

        this.title.innerHTML = this.current.format('MMMM YYYY');
    }

    drawMonth() {
        let self = this;

        this.events.forEach(function (ev) {
            ev.date = self.current.clone().date(Math.random() * (29 - 1) + 1);
        });


        if (this.month) {
            this.oldMonth = this.month;
            this.oldMonth.className = 'calendar-month out ' + (self.next ? 'next' : 'prev');
            this.oldMonth.addEventListener('webkitAnimationEnd', function () {
                self.oldMonth.parentNode.removeChild(self.oldMonth);
                self.month = createElement('div', 'calendar-month');
                self.backFill();
                self.currentMonth();
                self.fowardFill();
                self.el.appendChild(self.month);
                window.setTimeout(function () {
                    self.month.className = 'calendar-month in ' + (self.next ? 'next' : 'prev');
                }, 16);
            });
        } else {
            this.month = createElement('div', 'calendar-month');
            this.el.appendChild(this.month);
            this.backFill();
            this.currentMonth();
            this.fowardFill();
            this.month.className = 'calendar-month new';
        }
    }

    drawLegend() {
        let legend = createElement('div', 'calendar-legend');
        let calendars = this.events.map(function (e) {
            return e.calendar + '|' + e.color;
        }).reduce(function (memo, e) {
            if (memo.indexOf(e) === -1) {
                memo.push(e);
            }
            return memo;
        }, []).forEach(function (e) {
            let parts = e.split('|');
            let entry = createElement('span', 'calendar-entry ' + parts[1], parts[0]);
            legend.appendChild(entry);
        });
        this.el.appendChild(legend);
    }

    backFill() {
        let clone = this.current.clone();
        let dayOfWeek = clone.day();

        if (!dayOfWeek) {
            return;
        }

        clone.subtract(dayOfWeek + 1,'days');

        for (let i = dayOfWeek; i > 0; i--) {
            this.drawDay(clone.add(1,'days'));
        }
    }

    fowardFill() {
        let clone = this.current.clone().add(1,'months').subtract(1,'days');
        let dayOfWeek = clone.day();

        if (dayOfWeek === 6) {
            return;
        }

        for (let i = dayOfWeek; i < 6; i++) {
            this.drawDay(clone.add(1,'days'));
        }
    }

    currentMonth() {
        let clone = this.current.clone();

        while (clone.month() === this.current.month()) {
            this.drawDay(clone);
            clone.add(1,'days');
        }
    }

    getWeek(day) {
        if (!this.week || day.day() === 0) {
            this.week = createElement('div', 'calendar-week');
            this.month.appendChild(this.week);
        }
    }

    drawDay(day) {
        let self = this;
        this.getWeek(day);

        //Outer Day
        let outer = createElement('div', this.getDayClass(day));
        outer.addEventListener('click', function () {
            self.openDay(this);
        });

        //Day Name
        let name = createElement('div', 'calendar-day-name', day.format('ddd'));

        //Day Number
        let number = createElement('div', 'calendar-day-number', day.format('DD'));

        //Events
        let events = createElement('div', 'calendar-day-events');
        this.drawEvents(day, events);

        outer.appendChild(name);
        outer.appendChild(number);
        outer.appendChild(events);
        this.week.appendChild(outer);
    }

    drawEvents(day, element) {
        if (day.month() === this.current.month()) {
            let todaysEvents = this.events.reduce(function (memo, ev) {
                if (ev.date.isSame(day, 'day')) {
                    memo.push(ev);
                }
                return memo;
            }, []);

            todaysEvents.forEach(function (ev) {
                let evSpan = createElement('span', ev.color);
                element.appendChild(evSpan);
            });
        }
    }

    getDayClass(day) {
        let classes = ['day'];
        if (day.month() !== this.current.month()) {
            classes.push('other');
        } else if (this.today.isSame(day, 'day')) {
            classes.push('today');
        }
        return classes.join(' ');
    }

    openDay(el) {
        let details, arrow;
        let dayNumber = +el.querySelectorAll('.calendar-day-number')[0].innerText || +el.querySelectorAll('.day-number')[0].textContent;
        let day = this.current.clone().date(dayNumber);

        let currentOpened = document.querySelector('.calendar-details');

        //Check to see if there is an open detais box on the current row
        if (currentOpened && currentOpened.parentNode === el.parentNode) {
            details = currentOpened;
            arrow = document.querySelector('.calendar-arrow');
        } else {
            //Close the open events on differnt week row
            //currentOpened && currentOpened.parentNode.removeChild(currentOpened);
            if (currentOpened) {
                currentOpened.addEventListener('webkitAnimationEnd', function () {
                    currentOpened.parentNode.removeChild(currentOpened);
                });
                currentOpened.addEventListener('oanimationend', function () {
                    currentOpened.parentNode.removeChild(currentOpened);
                });
                currentOpened.addEventListener('msAnimationEnd', function () {
                    currentOpened.parentNode.removeChild(currentOpened);
                });
                currentOpened.addEventListener('animationend', function () {
                    currentOpened.parentNode.removeChild(currentOpened);
                });
                currentOpened.className = 'calendar-details out';
            }

            //Create the Details Container
            details = createElement('div', 'calendar-details in');

            //Create the arrow
            arrow = createElement('div', 'calendar-arrow');

            //Create the event wrapper

            details.appendChild(arrow);
            el.parentNode.appendChild(details);
        }

        let todaysEvents = this.events.reduce(function (memo, ev) {
            if (ev.date.isSame(day, 'day')) {
                memo.push(ev);
            }
            return memo;
        }, []);

        this.renderEvents(todaysEvents, details);

        arrow.style.left = el.offsetLeft - el.parentNode.offsetLeft + 27 + 'px';
    }

    renderEvents(events, ele) {
        //Remove any events in the current details element
        let currentWrapper = ele.querySelector('.calendar-events');
        let wrapper = createElement('div', 'calendar-events in' + (currentWrapper ? ' new' : ''));

        events.forEach(function (ev) {
            let div = createElement('div', 'calendar-event');
            let square = createElement('div', 'calendar-event-category ' + ev.color);
            let span = createElement('span', '', ev.eventName);

            div.appendChild(square);
            div.appendChild(span);
            wrapper.appendChild(div);
        });

        if (!events.length) {
            let div = createElement('div', 'calendar-event empty');
            let span = createElement('span', '', '没有安排');

            div.appendChild(span);
            wrapper.appendChild(div);
        }

        if (currentWrapper) {
            currentWrapper.className = 'calendar-events out';
            currentWrapper.addEventListener('webkitAnimationEnd', function () {
                currentWrapper.parentNode.removeChild(currentWrapper);
                ele.appendChild(wrapper);
            });
            currentWrapper.addEventListener('oanimationend', function () {
                currentWrapper.parentNode.removeChild(currentWrapper);
                ele.appendChild(wrapper);
            });
            currentWrapper.addEventListener('msAnimationEnd', function () {
                currentWrapper.parentNode.removeChild(currentWrapper);
                ele.appendChild(wrapper);
            });
            currentWrapper.addEventListener('animationend', function () {
                currentWrapper.parentNode.removeChild(currentWrapper);
                ele.appendChild(wrapper);
            });
        } else {
            ele.appendChild(wrapper);
        }
    }

    nextMonth() {
        this.current.add(1, 'months');
        this.next = true;
        this.draw();
    }

    prevMonth() {
        this.current.subtract(1, 'months');
        this.next = false;
        this.draw();
    }

}