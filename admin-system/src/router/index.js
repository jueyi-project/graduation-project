import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter);

const routes = [
    {
        path: '/',
        component: () => import('../views/login'),
        meta: {title: '登录'}
    },
    {
        path: '/system',
        component: () => import('../views/system'),
        redirect: '/system/workbench',
        children: [
            {
                path: 'workbench',
                meta: {title: '工作台'},
                component: () => import('../views/system/workbench')
            },
            {
                path: 'userAccountManagement',
                meta: {title: '用户账号管理'},
                component: () => import('../views/system/userManagement/userAccountManagement')
            },
            {
                path: 'merchantCertificationReview',
                meta: {title: '商家认证审核'},
                component: () => import('../views/system/userManagement/merchantCertificationReview')
            },
            {
                path: 'goods',
                meta: {title: '全部商品'},
                component: () => import('../views/system/goodsManagement/goods')

            },
            {
                path: 'productListingReview',
                meta: {title: '商品上架审核'},
                component: () => import('../views/system/goodsManagement/productListingReview')
            },
            {
                path: 'productReviewManagement',
                meta: {title: '商品评论管理'},
                component: () => import('../views/system/goodsManagement/productReviewManagement')
            },
            {
                path: 'commoditySalesStatistics',
                meta: {title: '商品销量统计'},
                component: () => import('../views/system/goodsManagement/commoditySalesStatistics')
            },
            {
                path: 'fullReductionActivityManagement',
                meta: {title: '满减活动管理'},
                component: () => import('../views/system/activityManagement/fullReductionActivityManagement')
            },
            {
                path: 'advertisingCampaignManagement',
                meta: {title: '广告活动管理'},
                component: () => import('../views/system/activityManagement/advertisingCampaignManagement')
            },
            {
                path: 'home',
                meta: {title: '个人中心'},
                component: () => import('../views/system/home')
            }
        ]
    },
    {
        path: '/404',
        meta: {title: '找不到了'},
        component: () => import('../views/404')
    },
    {
        path: '*',
        redirect: '/404'
    }
];
const router = new VueRouter({
    mode: 'history',
    base: process.env.BASE_URL,
    routes: routes
});

const originalPush = VueRouter.prototype.push;
VueRouter.prototype.push = function push(location) {
    return originalPush.call(this, location).catch(err => err);
};

// 路由判断登录 根据路由配置文件的参数
router.beforeEach((to, from, next) => {
    //判断是否有标题
    if (to.meta.title) {
        document.title = to.meta.title;
    } else {
        document.title = '佳品乐购';
    }
    next();
});


export default router
